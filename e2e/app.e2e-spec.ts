import { PairComponentPage } from './app.po';

describe('pair-component App', function() {
  let page: PairComponentPage;

  beforeEach(() => {
    page = new PairComponentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
