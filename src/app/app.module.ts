import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {AppComponent} from "./app.component";
import { NgFor } from '@angular/common';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';

@NgModule({
  declarations: [
    AppComponent,
    LeaderboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    NgxDatatableModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
