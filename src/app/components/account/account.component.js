var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 20/12/2016.
 */
var core_1 = require('@angular/core');
var accountComponent = (function () {
    function accountComponent(route, router) {
        this.route = route;
        this.router = router;
        this.showUpdatedToast = false;
    }
    accountComponent.prototype.onSelect = function (relativeRoute, event) {
        if (relativeRoute == "") {
            this.router.navigate(['account']);
        }
        else {
            this.router.navigate([relativeRoute], { relativeTo: this.route });
        }
        this.styleTags(event);
    };
    accountComponent.prototype.styleTags = function (event) {
        var allTags = event.target.offsetParent.parentElement.children;
        for (var t = 0; t < allTags.length; t++) {
            var tag = allTags[t];
            if (tag.classList.contains('active')) {
                tag.classList.remove(['active']);
            }
        }
        event.target.offsetParent.classList.add('active');
    };
    accountComponent.prototype.ngOnInit = function () {
    };
    accountComponent = __decorate([
        core_1.Component({
            selector: 'account-panel',
            templateUrl: 'account.template.html'
        })
    ], accountComponent);
    return accountComponent;
})();
exports.accountComponent = accountComponent;
//# sourceMappingURL=account.component.js.map