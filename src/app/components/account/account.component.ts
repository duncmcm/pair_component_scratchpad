/**
 * Created by duncmcm on 20/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'account-panel',
    templateUrl: 'account.template.html'
})

export class accountComponent implements OnInit {

    public showUpdatedToast:Boolean = false;

    constructor(private route: ActivatedRoute, private router: Router) {

    }

    onSelect(relativeRoute:string, event:any) {
        if (relativeRoute == "") {
            this.router.navigate(['account']);
        } else {
            this.router.navigate([relativeRoute], { relativeTo: this.route });
        }
        this.styleTags(event);
    }

    styleTags(event:any) {
        let allTags:Array<any> = event.target.offsetParent.parentElement.children;
        for (var t=0; t < allTags.length; t++) {
            let tag = allTags[t];
            if (tag.classList.contains('active')) { // Assume only one class
                tag.classList.remove(['active']);
            }
        }
        event.target.offsetParent.classList.add('active');
    }

    ngOnInit() {

    }
}