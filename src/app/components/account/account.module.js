var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 21/12/2016.
 */
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_1 = require('@angular/router');
var account_component_1 = require("./account.component");
var account_details_component_1 = require("./subcomponents/details/account.details.component");
var account_deposit_component_1 = require('./subcomponents/deposit/account.deposit.component');
var account_withdraw_component_1 = require('./subcomponents/withdraw/account.withdraw.component');
var account_transaction_component_1 = require('./subcomponents/transaction/account.transaction.component');
var account_timeonsite_component_1 = require('./subcomponents/timeonsite/account.timeonsite.component');
var account_exclude_component_1 = require('./subcomponents/exclude/account.exclude.component');
var account_limit_component_1 = require('./subcomponents/limit/account.limit.component');
var account_verification_component_1 = require('./subcomponents/verification/account.verification.component');
var accountRoutes = [
    {
        path: 'account',
        component: account_component_1.accountComponent,
        children: [
            { path: '', component: account_details_component_1.accountDetailsComponent },
            { path: 'deposit', component: account_deposit_component_1.accountDepositComponent },
            { path: 'withdraw', component: account_withdraw_component_1.accountWithdrawComponent },
            { path: 'transaction', component: account_transaction_component_1.accountTransactionComponent },
            { path: 'time_on_site', component: account_timeonsite_component_1.accountTimeonsiteComponent },
            { path: 'exclude', component: account_exclude_component_1.accountExcludeComponent },
            { path: 'limit', component: account_limit_component_1.accountLimitComponent },
            { path: 'verification', component: account_verification_component_1.accountVerificationComponent },
        ]
    }
];
var AccountModule = (function () {
    function AccountModule() {
    }
    AccountModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(accountRoutes),
                platform_browser_1.BrowserModule
            ],
            exports: [
                router_1.RouterModule
            ],
            declarations: [
                account_component_1.accountComponent,
                account_details_component_1.accountDetailsComponent,
                account_deposit_component_1.accountDepositComponent,
                account_withdraw_component_1.accountWithdrawComponent,
                account_transaction_component_1.accountTransactionComponent,
                account_timeonsite_component_1.accountTimeonsiteComponent,
                account_exclude_component_1.accountExcludeComponent,
                account_limit_component_1.accountLimitComponent,
                account_verification_component_1.accountVerificationComponent
            ]
        })
    ], AccountModule);
    return AccountModule;
})();
exports.AccountModule = AccountModule;
//# sourceMappingURL=account.module.js.map