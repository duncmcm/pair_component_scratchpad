/**
 * Created by duncmcm on 21/12/2016.
 */
import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { accountComponent } from "./account.component";
import { accountDetailsComponent } from "./subcomponents/details/account.details.component";
import { accountDepositComponent } from './subcomponents/deposit/account.deposit.component';
import { accountWithdrawComponent } from './subcomponents/withdraw/account.withdraw.component';
import { accountTransactionComponent } from './subcomponents/transaction/account.transaction.component';
import { accountTimeonsiteComponent } from './subcomponents/timeonsite/account.timeonsite.component';
import { accountExcludeComponent } from './subcomponents/exclude/account.exclude.component';
import { accountLimitComponent } from './subcomponents/limit/account.limit.component';
import { accountVerificationComponent } from './subcomponents/verification/account.verification.component';

import { NgFor } from '@angular/common';

const accountRoutes: Routes = [
    {
        path: 'account',
        component: accountComponent,
        children: [
            { path: '', component: accountDetailsComponent},
            { path: 'deposit', component: accountDepositComponent},
            { path: 'withdraw', component: accountWithdrawComponent},
            { path: 'transaction', component: accountTransactionComponent},
            { path: 'time_on_site', component: accountTimeonsiteComponent},
            { path: 'exclude', component: accountExcludeComponent},
            { path: 'limit', component: accountLimitComponent},
            { path: 'verification', component: accountVerificationComponent},
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(accountRoutes),
        BrowserModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        accountComponent,
        accountDetailsComponent,
        accountDepositComponent,
        accountWithdrawComponent,
        accountTransactionComponent,
        accountTimeonsiteComponent,
        accountExcludeComponent,
        accountLimitComponent,
        accountVerificationComponent
    ],
})
export class AccountModule { }
