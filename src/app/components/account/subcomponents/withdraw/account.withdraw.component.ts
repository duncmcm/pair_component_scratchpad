/**
 * Created by duncmcm on 21/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'account-withdraw',
    templateUrl: 'account.withdraw.template.html',
    directives: [NgFor],
})

export class accountWithdrawComponent implements OnInit {

    constructor(private route: ActivatedRoute, private router: Router) {

    }

    ngOnInit() {

    }

    goToDepositPage() {
        this.router.navigate(['deposit']);
    }
}