var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 12/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var prize_panel_component_1 = require('../subcomponents/panels/prize/prize.panel.component');
var entries_panel_component_1 = require('../subcomponents/panels/entries/entries.panel.component');
var fixtures_panel_component_1 = require('../subcomponents/panels/fixture/fixtures.panel.component');
var availableContestDetails = (function () {
    function availableContestDetails() {
        this.onClose = new core_1.EventEmitter();
        this.buttons = ['Prizes', 'Fixtures', 'Entries'];
        this.selection = 1;
    }
    availableContestDetails.prototype.ngOnInit = function () {
    };
    availableContestDetails.prototype.panelSelected = function (e) {
        console.log('panel selected ' + e);
        switch (e.target.innerHTML) {
            case 'Prizes':
                this.selection = 1;
                break;
            case 'Fixtures':
                this.selection = 2;
                break;
            case 'Entries':
                this.selection = 3;
                break;
            default:
                this.selection = 1;
                break;
        }
    };
    availableContestDetails.prototype.close = function () {
        this.onClose.emit(true);
    };
    __decorate([
        core_1.Input()
    ], availableContestDetails.prototype, "contest");
    __decorate([
        core_1.Output()
    ], availableContestDetails.prototype, "onClose");
    availableContestDetails = __decorate([
        core_1.Component({
            selector: 'available-contest-details-panel',
            templateUrl: 'available.contest.details.template.html',
            styleUrls: ['available.contest.details.scss'],
            directives: [common_1.NgFor, prize_panel_component_1.prizePanelComponent, fixtures_panel_component_1.fixturesPanelComponent, entries_panel_component_1.entriesPanelComponent]
        })
    ], availableContestDetails);
    return availableContestDetails;
})();
exports.availableContestDetails = availableContestDetails;
//# sourceMappingURL=available.contest.details.js.map