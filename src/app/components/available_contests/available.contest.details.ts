/**
 * Created by duncmcm on 12/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { Contest } from '../../models/contest';
import { prizePanelComponent } from '../subcomponents/panels/prize/prize.panel.component';
import { entriesPanelComponent } from '../subcomponents/panels/entries/entries.panel.component';
import { fixturesPanelComponent } from '../subcomponents/panels/fixture/fixtures.panel.component';

@Component({
    selector: 'available-contest-details-panel',
    templateUrl: 'available.contest.details.template.html',
    styleUrls: ['available.contest.details.scss'],
    directives: [NgFor, prizePanelComponent, fixturesPanelComponent, entriesPanelComponent],
})

export class availableContestDetails implements OnInit {

    @Input() contest:Contest;
    @Output() onClose = new EventEmitter<boolean>();
    buttons:Array<string> = ['Prizes', 'Fixtures', 'Entries'];
    selection:number = 1;

    constructor() {
    }

    ngOnInit() {

    }

    panelSelected(e) {
        console.log('panel selected ' +e);
        switch (e.target.innerHTML) {
            case 'Prizes':
                this.selection = 1;
                break;
            case 'Fixtures':
                this.selection = 2;
                break;
            case 'Entries':
                this.selection = 3;
                break;
            default:
                this.selection = 1;
                break;
        }
    }

    close() {
        this.onClose.emit(true);
    }
}