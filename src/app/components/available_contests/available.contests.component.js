var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 07/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var available_contest_details_1 = require('./available.contest.details');
var availableContestComponent = (function () {
    function availableContestComponent(zone, contestService, userTeamService, ref) {
        this.zone = zone;
        this.dummy = false;
        this.contests = [];
        this.showDetails = false;
        this.contestService = contestService;
        this.userTeamService = userTeamService;
        this.ref = ref;
    }
    availableContestComponent.prototype.ngOnInit = function () {
    };
    availableContestComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.contests = [];
        this.subscription = this.contestService.contestAnnounce.subscribe(function (contest) {
            _this.zone.run(function () {
                _this.contests.push(contest);
                console.log('contest received in Available Contest Panel ' + contest.name);
                _this.ref.detectChanges();
            });
        });
        this.getAllContests();
    };
    availableContestComponent.prototype.showContestDetails = function (contest) {
        this.showDetails = true;
        this.selectedContest = contest;
    };
    availableContestComponent.prototype.closeDetails = function () {
        this.showDetails = false;
    };
    availableContestComponent.prototype.getAllContests = function () {
        if (this.dummy) {
            this.contestService.getDummyData();
        }
        else {
            this.contestService.getContests();
        }
    };
    availableContestComponent.prototype.ngOnDestroy = function () {
        console.log('Available Contest Destroyed');
        this.subscription = null;
    };
    __decorate([
        core_1.Input()
    ], availableContestComponent.prototype, "contests");
    availableContestComponent = __decorate([
        core_1.Component({
            selector: 'available-contest-panel',
            templateUrl: 'available.contests.template.html',
            styleUrls: ['available.contests.scss'],
            directives: [common_1.NgFor, available_contest_details_1.availableContestDetails]
        })
    ], availableContestComponent);
    return availableContestComponent;
})();
exports.availableContestComponent = availableContestComponent;
//# sourceMappingURL=available.contests.component.js.map