/**
 * Created by duncmcm on 07/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, ChangeDetectorRef, OnDestroy, Input, NgZone, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { NgFor } from '@angular/common';
import {ContestService} from "../../services/contestService";
import { Contest } from '../../models/contest';
import {FixtureService} from "../../services/fixtureService";
import { availableContestDetails } from './available.contest.details';
import { Observable, Subscription } from 'rxjs';
import {UserTeamService} from "../../services/UserTeamService";

@Component({
    selector: 'available-contest-panel',
    templateUrl: 'available.contests.template.html',
    styleUrls: ['available.contests.scss'],
    directives: [NgFor, availableContestDetails]
})

export class availableContestComponent implements OnInit, OnDestroy {

    dummy:Boolean = false;
    @Input() contests:Contest[] = [];
    selectedContest:Contest;

    contestService:ContestService;
    userTeamService:UserTeamService;

    public showDetails:Boolean =false;
    public subscription:Subscription;
    public ref:ChangeDetectorRef;

    constructor(private zone:NgZone, contestService:ContestService, userTeamService:UserTeamService, ref: ChangeDetectorRef) {
        this.contestService = contestService;
        this.userTeamService = userTeamService;
        this.ref = ref;
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.contests = [];
        this.subscription = this.contestService.contestAnnounce.subscribe(
            contest => {
                this.zone.run(() => {
                    this.contests.push(contest);
                    console.log('contest received in Available Contest Panel ' +contest.name);
                    this.ref.detectChanges();
                })
            });
        this.getAllContests();
    }

    showContestDetails(contest:Contest) {
        this.showDetails = true;
        this.selectedContest = contest;
    }

    closeDetails() {
        this.showDetails = false;
    }

    getAllContests() {
        if (this.dummy) {
            this.contestService.getDummyData();
        } else {
            this.contestService.getContests();
        }
    }

    ngOnDestroy() {
        console.log('Available Contest Destroyed');
        this.subscription = null;
    }
}