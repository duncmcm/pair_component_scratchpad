/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { NgFor } from '@angular/common';
import { Observable, Subscription } from 'rxjs';

@Component({
    templateUrl: 'base.template.html',
    directives: [NgFor],
})

export class baseComponent implements OnInit, OnDestroy {

    public subscription1:Subscription;
    public subscription2:Subscription;
    public subscription3:Subscription;

    constructor() {

    }

    ngOnInit() {

    }

    ngOnDestroy() {  // Need typescript update
        if (this.subscription1 != null) {
            //this.subscription1.unsubscribe();
        }
        if (this.subscription2 != null) {
            //this.subscription2.unsubscribe();
        }
        if (this.subscription3 != null) {
            //this.subscription3.unsubscribe();
        }
    }
}