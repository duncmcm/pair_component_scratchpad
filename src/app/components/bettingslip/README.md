NOTES
=====

It may be a possibility to dynamically change the template being used by each component to a 'mobile'
version. So I have produced a 'betting.slip.template.mobile.html' file in this directory as a trial run.
 
We can then use a component base class to switch this template depending on '$window' settings as used in
<ff-component-wrapper>'

TBC


=====


