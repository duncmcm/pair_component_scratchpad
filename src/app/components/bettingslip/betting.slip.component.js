var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 23/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var _ = require('underscore');
var bettingSlipComponent = (function () {
    function bettingSlipComponent(service, userTeamService, contestService, ref) {
        var _this = this;
        this.accessBetslip = true;
        this.freeCredit = true;
        this.disabled = true;
        this.total = 0;
        this.teams = [];
        /**
         * set this to false if mobile
         * @type {boolean}
         */
        this.panel = true;
        this.userService = service;
        this.userTeamService = userTeamService;
        this.contestService = contestService;
        this.ref = ref;
        this.subscription2 = this.userTeamService.teamAnnounce.subscribe(function (team) {
            var ct = _.find(_this.contestService.contests, function (contest) {
                return contest.id == team.contest.id;
            });
            if (ct != null) {
                team.entryFee = ct.entryFee;
                _this.total += team.entryFee;
            }
            _this.teams.push(team);
            _this.ref.detectChanges();
            console.log('team received in Betting Slip Panel with name ' + team.name);
        });
    }
    bettingSlipComponent.prototype.ngOnInit = function () {
        console.log('Betting slip init');
        this.getUserTeams();
    };
    bettingSlipComponent.prototype.getUserTeams = function () {
        if (this.userService.loggedInUser) {
            this.userTeamService.getUserTeamEntriesInBettingSlip(this.userService.loggedInUser.bettingSlip.id);
        }
    };
    bettingSlipComponent.prototype.postBetslip = function () {
        //ToDo - Post betslip to DB
    };
    bettingSlipComponent.prototype.ngOnDestroy = function () {
        console.log('Betting slip detroyed');
        this.subscription2 = null;
    };
    bettingSlipComponent = __decorate([
        core_1.Component({
            selector: 'betting-slip',
            templateUrl: 'betting.slip.template.html',
            styleUrls: ['betting.slip.scss'],
            directives: [common_1.NgFor]
        })
    ], bettingSlipComponent);
    return bettingSlipComponent;
})();
exports.bettingSlipComponent = bettingSlipComponent;
//# sourceMappingURL=betting.slip.component.js.map