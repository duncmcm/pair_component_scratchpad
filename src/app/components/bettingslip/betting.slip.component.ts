/**
 * Created by duncmcm on 23/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, OnChanges, AfterViewInit, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { NgFor, AsyncPipe } from '@angular/common';
import {UserItem} from "../../models/user";
import {UserTeam} from "../../models/userteam";
import {UserService} from "../../services/userService";
import {UserTeamService} from "../../services/UserTeamService";
import {ContestService} from "../../services/contestService";
import * as _ from 'underscore';
import {Contest} from "../../models/contest";
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'betting-slip',
    templateUrl: 'betting.slip.template.html',
    styleUrls:['betting.slip.scss'],
    directives: [NgFor]
})

export class bettingSlipComponent implements OnInit , OnDestroy {

    public accessBetslip:Boolean = true;
    public user:UserItem;
    public freeCredit:Boolean = true;
    public disabled:Boolean = true;
    public total:number = 0;

    teams:Array<UserTeam> = [];
    /**
     * set this to false if mobile
     * @type {boolean}
     */
    public panel:Boolean = true;
    public userService:UserService;
    public userTeamService:UserTeamService;
    public contestService:ContestService;
    public subscription2:Subscription;
    public ref:ChangeDetectorRef;

    constructor(service:UserService, userTeamService:UserTeamService, contestService:ContestService, ref: ChangeDetectorRef) {
        this.userService = service;
        this.userTeamService = userTeamService;
        this.contestService = contestService;
        this.ref = ref;
        this.subscription2 = this.userTeamService.teamAnnounce.subscribe(
            team => {

                let ct:Contest = _.find(this.contestService.contests, function(contest) {
                    return contest.id == team.contest.id;
                });
                if (ct != null) {
                    team.entryFee = ct.entryFee;
                    this.total += team.entryFee;
                }

                this.teams.push(team);
                this.ref.detectChanges();
                console.log('team received in Betting Slip Panel with name ' +team.name);

            });
    }

    ngOnInit() {
        console.log('Betting slip init');
        this.getUserTeams();
    }

    getUserTeams() {
        if (this.userService.loggedInUser) {
            this.userTeamService.getUserTeamEntriesInBettingSlip(this.userService.loggedInUser.bettingSlip.id);
        }
    }

    postBetslip() {
        //ToDo - Post betslip to DB
    }

    ngOnDestroy() {
        console.log('Betting slip detroyed');
        this.subscription2 = null;
    }
}