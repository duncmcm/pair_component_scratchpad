/**
 * Created by duncmcm on 20/12/2016.
 */
import {Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'faq-panel',
    templateUrl: 'faq.template.html'
})

export class faqComponent implements OnInit {

    @Output() removeFaq = new EventEmitter<boolean>();

    constructor() {
    }

    ngOnInit() {

    }

    removeFAQ() {
        this.removeFaq.emit(true);
    }
}