var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 29/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var gameClosedComponent = (function () {
    function gameClosedComponent() {
    }
    gameClosedComponent.prototype.ngOnInit = function () {
    };
    gameClosedComponent = __decorate([
        core_1.Component({
            selector: 'game-closed',
            templateUrl: 'game.closed.template.html',
            styleUrls: ['game.closed.scss'],
            directives: [common_1.NgFor]
        })
    ], gameClosedComponent);
    return gameClosedComponent;
})();
exports.gameClosedComponent = gameClosedComponent;
//# sourceMappingURL=game.closed.component.js.map