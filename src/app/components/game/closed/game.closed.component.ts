/**
 * Created by duncmcm on 29/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';

@Component({
    selector: 'game-closed',
    templateUrl: 'game.closed.template.html',
    styleUrls:['game.closed.scss'],
    directives: [NgFor],
})

export class gameClosedComponent implements OnInit {

    constructor() {

    }

    ngOnInit() {

    }
}