var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 15/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var team_players_component_1 = require('./subcomponents/teamplayers/team.players.component');
var livegames_component_1 = require('./subcomponents/livegames/livegames.component');
var contest_1 = require("../../models/contest");
var leaderboard_component_1 = require('./subcomponents/leaderboard/leaderboard.component');
var toppanel_component_1 = require('./subcomponents/toppanel/toppanel.component');
var subheader_component_1 = require('./subcomponents/subheader/subheader.component');
var gameComponent = (function () {
    function gameComponent(service) {
        /**
         * edit = true only if mode = 'OPEN'
         * live = true only if mode = 'LIVE'
         * mode = 'CLOSED', 'OPEN' or 'LIVE'
         * @type {boolean}
         */
        this.edit = true;
        this.live = false;
        this.mode = 'OPEN';
        this.typeId = 1;
        this.contestService = service;
    }
    gameComponent.prototype.ngOnInit = function () {
        this.contest = this.contestService.selectedContest;
        this.mode = this.contestService.selectedContest.status.toLocaleUpperCase();
    };
    gameComponent.prototype.ngOnChanges = function (changes) {
        for (var propName in changes) {
            var changedProp = changes[propName];
            if (changedProp.currentValue instanceof contest_1.Contest) {
                this.contest = changedProp.currentValue;
                console.log('contest name in games changed' + changedProp.currentValue.name);
            }
        }
    };
    gameComponent = __decorate([
        core_1.Component({
            selector: 'game',
            templateUrl: 'game.ongoing.template.html',
            directives: [common_1.NgFor, team_players_component_1.teamPlayersComponent, livegames_component_1.liveGamesComponent, leaderboard_component_1.leaderboardComponent,
                toppanel_component_1.topPanelComponent, subheader_component_1.subheaderComponent]
        })
    ], gameComponent);
    return gameComponent;
})();
exports.gameComponent = gameComponent;
//# sourceMappingURL=game.component.js.map