/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChange } from '@angular/core';
import { NgFor } from '@angular/common';
import { teamPlayersComponent } from './subcomponents/teamplayers/team.players.component';
import { liveGamesComponent } from './subcomponents/livegames/livegames.component';
import {Contest} from "../../models/contest";
import { leaderboardComponent } from './subcomponents/leaderboard/leaderboard.component'
import { topPanelComponent } from './subcomponents/toppanel/toppanel.component';
import { subheaderComponent } from './subcomponents/subheader/subheader.component';
import {ContestService} from "../../services/contestService";

@Component({
    selector: 'game',
    templateUrl: 'game.ongoing.template.html',
    directives: [NgFor, teamPlayersComponent, liveGamesComponent, leaderboardComponent,
                 topPanelComponent, subheaderComponent],
})

export class gameComponent implements OnInit, OnChanges {

    /**
     * edit = true only if mode = 'OPEN'
     * live = true only if mode = 'LIVE'
     * mode = 'CLOSED', 'OPEN' or 'LIVE'
     * @type {boolean}
     */
    public edit:Boolean = true;
    public live:Boolean = false;
    public mode:string = 'OPEN';
    public typeId:number = 1;

    contest:Contest;
    public contestService:ContestService;

    constructor(service:ContestService) {
        this.contestService = service;
    }

    ngOnInit() {
        this.contest = this.contestService.selectedContest;
        this.mode = this.contestService.selectedContest.status.toLocaleUpperCase();
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        for (let propName in changes) {
            let changedProp = changes[propName];
            if (changedProp.currentValue instanceof Contest) {
                this.contest = changedProp.currentValue;
                console.log('contest name in games changed' +changedProp.currentValue.name);
            }
        }
    }
}