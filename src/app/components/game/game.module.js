var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 29/12/2016.
 */
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_1 = require('@angular/router');
var game_component_1 = require('./game.component');
var game_closed_component_1 = require('./closed/game.closed.component');
var game_open_component_1 = require('./open/game.open.component');
var game_live_component_1 = require('./live/game.live.component');
var game_rules_component_1 = require('./rules/game.rules.component');
var shared_module_1 = require('../shared/shared.module');
var gameRoutes = [
    {
        path: 'contest',
        component: game_component_1.gameComponent,
        children: [
            { path: '', component: game_open_component_1.gameOpenComponent },
            { path: 'live', component: game_live_component_1.gameLiveComponent },
            { path: 'closed', component: game_closed_component_1.gameClosedComponent },
            { path: 'rules', component: game_rules_component_1.gameRulesComponent },
        ]
    }
];
var GameModule = (function () {
    function GameModule() {
    }
    GameModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(gameRoutes),
                platform_browser_1.BrowserModule,
                shared_module_1.SharedModule
            ],
            exports: [
                router_1.RouterModule
            ],
            declarations: [
                game_component_1.gameComponent,
                game_open_component_1.gameOpenComponent,
                game_live_component_1.gameLiveComponent,
                game_closed_component_1.gameClosedComponent,
                game_rules_component_1.gameRulesComponent
            ]
        })
    ], GameModule);
    return GameModule;
})();
exports.GameModule = GameModule;
//# sourceMappingURL=game.module.js.map