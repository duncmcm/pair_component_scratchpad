/**
 * Created by duncmcm on 29/12/2016.
 */
import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { gameComponent } from './game.component';
import { gameClosedComponent } from './closed/game.closed.component'
import { gameOpenComponent } from './open/game.open.component';
import { gameLiveComponent } from './live/game.live.component';
import { gameRulesComponent } from './rules/game.rules.component';
import { SharedModule } from '../shared/shared.module';

const gameRoutes: Routes = [
    {
        path: 'contest',
        component: gameComponent,
        children: [
            { path: '', component: gameOpenComponent},
            { path: 'live', component: gameLiveComponent},
            { path: 'closed', component: gameClosedComponent},
            { path: 'rules', component: gameRulesComponent},
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(gameRoutes),
        BrowserModule,
        SharedModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        gameComponent,
        gameOpenComponent,
        gameLiveComponent,
        gameClosedComponent,
        gameRulesComponent
    ],
})
export class GameModule { }