var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 29/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var filter_player_component_1 = require('../subcomponents/filterplayer/filter.player.component');
var gameOpenComponent = (function () {
    function gameOpenComponent() {
    }
    gameOpenComponent.prototype.ngOnInit = function () {
    };
    gameOpenComponent = __decorate([
        core_1.Component({
            selector: 'game-open',
            templateUrl: 'game.open.template.html',
            styleUrls: ['game.open.scss'],
            directives: [common_1.NgFor, filter_player_component_1.filterPlayerComponent]
        })
    ], gameOpenComponent);
    return gameOpenComponent;
})();
exports.gameOpenComponent = gameOpenComponent;
//# sourceMappingURL=game.open.component.js.map