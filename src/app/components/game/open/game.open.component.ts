/**
 * Created by duncmcm on 29/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { filterPlayerComponent } from '../subcomponents/filterplayer/filter.player.component';

@Component({
    selector: 'game-open',
    templateUrl: 'game.open.template.html',
    styleUrls: ['game.open.scss'],
    directives: [NgFor, filterPlayerComponent],
})

export class gameOpenComponent implements OnInit {

    constructor() {

    }

    ngOnInit() {

    }
}