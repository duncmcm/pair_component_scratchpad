var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 29/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var gameRulesComponent = (function () {
    function gameRulesComponent() {
    }
    gameRulesComponent.prototype.ngOnInit = function () {
    };
    gameRulesComponent = __decorate([
        core_1.Component({
            selector: 'game-rules',
            templateUrl: 'game.rules.template.html',
            directives: [common_1.NgFor]
        })
    ], gameRulesComponent);
    return gameRulesComponent;
})();
exports.gameRulesComponent = gameRulesComponent;
//# sourceMappingURL=game.rules.component.js.map