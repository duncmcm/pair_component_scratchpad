var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 15/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var filterPlayerComponent = (function () {
    function filterPlayerComponent() {
        this.tiles = [
            { text: 'All Players', cols: 2, rows: 1, color: 'green' },
            { text: 'GK', cols: 1, rows: 1, color: 'lightblue' },
            { text: 'DEF', cols: 1, rows: 1, color: 'lightblue' },
            { text: 'MID', cols: 1, rows: 1, color: 'lightblue' },
            { text: 'FWD', cols: 1, rows: 1, color: 'lightblue' },
        ];
    }
    filterPlayerComponent.prototype.ngOnInit = function () {
    };
    filterPlayerComponent = __decorate([
        core_1.Component({
            selector: 'filter-player',
            templateUrl: 'filter.player.template.html',
            directives: [common_1.NgFor]
        })
    ], filterPlayerComponent);
    return filterPlayerComponent;
})();
exports.filterPlayerComponent = filterPlayerComponent;
//# sourceMappingURL=filter.player.component.js.map