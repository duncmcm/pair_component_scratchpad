/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';

@Component({
    selector: 'filter-player',
    templateUrl: 'filter.player.template.html',

    directives: [NgFor],
})

export class filterPlayerComponent implements OnInit {


    tiles: any[] = [
        {text: 'All Players', cols: 2, rows: 1, color: 'green'},
        {text: 'GK', cols: 1, rows: 1, color: 'lightblue'},
        {text: 'DEF', cols: 1, rows: 1, color: 'lightblue'},
        {text: 'MID', cols: 1, rows: 1, color: 'lightblue'},
        {text: 'FWD', cols: 1, rows: 1, color: 'lightblue'},
    ];

    constructor() {

    }

    ngOnInit() {

    }
}