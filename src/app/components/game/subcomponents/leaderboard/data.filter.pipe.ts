/**
 * Created by duncmcm on 20/12/2016.
 */
//import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {

    transform(array: any[], query: string): any {
        if (query) {
            return null; //_.filter(array, row=>row.name.indexOf(query) > -1);
        }
        return array;
    }
}