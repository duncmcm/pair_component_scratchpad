var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 19/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var baseComponent_1 = require("../../../base/baseComponent");
var leaderboardComponent = (function (_super) {
    __extends(leaderboardComponent, _super);
    function leaderboardComponent(contestService, ref) {
        var _this = this;
        _super.call(this);
        this.ref = ref;
        this.teams = [];
        this.rows = [];
        this.contestService = contestService;
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    leaderboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription1 = this.contestService.contestUserTeamsAnnounce.subscribe(function (team) {
            _this.teams.push(team);
            console.log('team received in Leaderboard Panel ' + team.name);
            _this.ref.detectChanges();
        });
        this.getUserTeamsForContest();
    };
    leaderboardComponent.prototype.getUserTeamsForContest = function () {
        this.contestService.getUserTeamsForContest(this.contestService.selectedContest.id);
    };
    leaderboardComponent.prototype.onPage = function (event) {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
            console.log('paged!', event);
        }, 100);
    };
    leaderboardComponent.prototype.onDummySelected = function (event) {
        var _this = this;
        this.fetch(function (data) {
            _this.rows = data;
        });
    };
    leaderboardComponent.prototype.onLiveSelected = function (event) {
        this.rows = this.teams;
    };
    leaderboardComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "app/components/game/subcomponents/leaderboard/data/100k.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    leaderboardComponent = __decorate([
        core_1.Component({
            selector: 'leaderboard',
            templateUrl: 'leaderboard.ngx.template.html',
            styleUrls: ['leaderboard.scss', 'datatable.scss', 'material.scss', 'icons.scss'],
            directives: [common_1.NgFor]
        })
    ], leaderboardComponent);
    return leaderboardComponent;
})(baseComponent_1.baseComponent);
exports.leaderboardComponent = leaderboardComponent;
//# sourceMappingURL=leaderboard.component.js.map