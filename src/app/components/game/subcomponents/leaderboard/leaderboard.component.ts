/**
 * Created by duncmcm on 19/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, ChangeDetectorRef } from '@angular/core';
import { NgFor } from '@angular/common';
import {ContestService} from "../../../../services/contestService";
import {UserTeam} from "../../../../models/userteam";
import {baseComponent} from "../../../base/baseComponent";

@Component({
    selector: 'leaderboard',
    templateUrl: 'leaderboard.ngx.template.html',
    styleUrls:['leaderboard.scss', 'datatable.scss', 'material.scss', 'icons.scss'],
    directives: [NgFor]
})

export class leaderboardComponent extends baseComponent implements OnInit {

    public contestService:ContestService;
    public teams:Array<UserTeam> = [];
    rows =  [];

    constructor(contestService:ContestService, private ref:ChangeDetectorRef) {
        super();
        this.contestService = contestService;

        this.fetch((data) => {
            this.rows = data;
        });
    }


    ngOnInit() {
        this.subscription1 = this.contestService.contestUserTeamsAnnounce.subscribe(
            team => {
                this.teams.push(team);
                console.log('team received in Leaderboard Panel ' +team.name);
                this.ref.detectChanges();
            });

        this.getUserTeamsForContest();
    }

    getUserTeamsForContest() {
        this.contestService.getUserTeamsForContest(this.contestService.selectedContest.id)
    }

    timeout:any;

    onPage(event) {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            console.log('paged!', event);
        }, 100);
    }

    onDummySelected(event) {
        this.fetch((data) => {
            this.rows = data;
        });
    }

    onLiveSelected(event) {
        this.rows = this.teams;
    }

    fetch(cb) {
        let req = new XMLHttpRequest();
        req.open('GET', `app/components/game/subcomponents/leaderboard/data/100k.json`);

        req.onload = () => {
            cb(JSON.parse(req.response));
        };

        req.send();
    }

}