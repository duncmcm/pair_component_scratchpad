var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 15/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var contestService_1 = require("../../../../services/contestService");
var fixtureService_1 = require("../../../../services/fixtureService");
var baseComponent_1 = require("../../../base/baseComponent");
var live_fixture_component_1 = require('../../../subcomponents/elements/live-fixture/live.fixture.component');
var liveGamesComponent = (function (_super) {
    __extends(liveGamesComponent, _super);
    function liveGamesComponent(s1, s2, ref) {
        var _this = this;
        _super.call(this);
        this.liveFixtures = [];
        this.contestService = s1;
        this.fixtureService = s2;
        this.subscription = this.fixtureService.fixtureAnnounce.subscribe(function (fixture) {
            _this.liveFixtures.push(fixture);
            console.log('Live fixture received in Panel ' + fixture.teamNameAwayAbbreviation);
            ref.detectChanges();
        });
    }
    liveGamesComponent.prototype.ngOnInit = function () {
        this.fixtureService.getFixtureWithStatus('Upcoming');
    };
    liveGamesComponent.prototype.ngOnChanges = function () {
        //console.log('contest name in live games ' +this.contest.name);
    };
    __decorate([
        core_1.Input()
    ], liveGamesComponent.prototype, "state");
    __decorate([
        core_1.Input()
    ], liveGamesComponent.prototype, "contest");
    liveGamesComponent = __decorate([
        core_1.Component({
            selector: 'live-games',
            templateUrl: 'livegames.template.html',
            directives: [common_1.NgFor, live_fixture_component_1.liveFixtureComponent],
            providers: [contestService_1.ContestService, fixtureService_1.FixtureService]
        })
    ], liveGamesComponent);
    return liveGamesComponent;
})(baseComponent_1.baseComponent);
exports.liveGamesComponent = liveGamesComponent;
//# sourceMappingURL=livegames.component.js.map