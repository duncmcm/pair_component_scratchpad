/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, OnChanges, ChangeDetectorRef } from '@angular/core';
import { NgFor } from '@angular/common';
import {ContestService} from "../../../../services/contestService";
import {FixtureService} from "../../../../services/fixtureService";
import {baseComponent} from "../../../base/baseComponent";
import {Fixture} from "../../../../models/fixture";
import {Contest} from "../../../../models/contest";
import { Observable, Subscription } from 'rxjs';
import { liveFixtureComponent } from '../../../subcomponents/elements/live-fixture/live.fixture.component';

@Component({
    selector: 'live-games',
    templateUrl: 'livegames.template.html',
    directives: [NgFor, liveFixtureComponent],
    providers:[ContestService, FixtureService]
})

export class liveGamesComponent extends baseComponent implements OnInit, OnChanges {

    @Input() state:Boolean;
    @Input() contest:Contest;

    public contestService:ContestService;
    public fixtureService:FixtureService;
    public liveFixtures:Fixture[] = [];
    public subscription:Subscription;

    constructor(s1:ContestService, s2:FixtureService, ref:ChangeDetectorRef) {
        super();
        this.contestService = s1;
        this.fixtureService = s2;

        this.subscription = this.fixtureService.fixtureAnnounce.subscribe(
            fixture => {
                this.liveFixtures.push(fixture);
                console.log('Live fixture received in Panel ' +fixture.teamNameAwayAbbreviation);
                ref.detectChanges();
            });
    }

    ngOnInit() {
        this.fixtureService.getFixtureWithStatus('Upcoming');
    }

    ngOnChanges() {
        //console.log('contest name in live games ' +this.contest.name);
    }
}