var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 15/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var subheaderComponent = (function () {
    function subheaderComponent() {
    }
    subheaderComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input()
    ], subheaderComponent.prototype, "contest");
    subheaderComponent = __decorate([
        core_1.Component({
            selector: 'subheader',
            templateUrl: 'subheader.template.html',
            directives: [common_1.NgFor]
        })
    ], subheaderComponent);
    return subheaderComponent;
})();
exports.subheaderComponent = subheaderComponent;
//# sourceMappingURL=subheader.component.js.map