/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import {Contest} from "../../../../models/contest";

@Component({
    selector: 'subheader',
    templateUrl: 'subheader.template.html',
    directives: [NgFor],
})

export class subheaderComponent implements OnInit {

    @Input() contest:Contest;

    constructor() {

    }

    ngOnInit() {

    }
}