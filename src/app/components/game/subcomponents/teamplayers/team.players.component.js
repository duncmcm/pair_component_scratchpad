var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 15/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var playerService_1 = require("../../../../services/playerService");
var baseComponent_1 = require("../../../base/baseComponent");
var UserTeamService_1 = require("../../../../services/UserTeamService");
var teamPlayersComponent = (function (_super) {
    __extends(teamPlayersComponent, _super);
    function teamPlayersComponent(service, userService, ref) {
        var _this = this;
        _super.call(this);
        this.players = [];
        this.playersInUserTeam = [];
        this.getTeams = [];
        this.open = false;
        this.submit_disabled = true;
        var that = this;
        this.playerService = service;
        this.userTeamService = userService;
        this.subscription1 = this.playerService.playerAnnounce.subscribe(function (player) {
            _this.playersInUserTeam.push(player);
            console.log('player received in Team Player Panel ' + player.lastName);
            _this.processPlayerInfo();
            ref.detectChanges();
        });
        this.subscription3 = this.playerService.allPlayerAnnounce.subscribe(function (status) {
            //this.players.push(player);
            console.log('All Players received ' + status);
            ref.detectChanges();
        });
        this.subscription2 = this.userTeamService.teamAnnounce.subscribe(function (team) {
            _this.team = team;
            console.log('team received in Team Player Panel ' + team.name);
            ref.detectChanges();
            _this.getPlayerInfoForTeam(_this.team);
        });
        //setInterval(function(){
        //    that.getPlayerInfoForTeam(that.team);
        //}, 5000);
    }
    teamPlayersComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
        this.getUserTeamWithId('xx32YGtlxm');
    };
    /** Player Info Business Login **/
    teamPlayersComponent.prototype.processPlayerInfo = function () {
        if (this.playersInUserTeam.length != 5) {
            this.submit_disabled = true;
            return;
        }
        this.submit_disabled = false;
        this.formatTeamPoints();
    };
    teamPlayersComponent.prototype.formatTeamPoints = function () {
        this.teamPoints = this.round(this.team.points, 1).toFixed(1);
        console.log(' team points = ' + this.teamPoints);
    };
    teamPlayersComponent.prototype.round = function (value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    };
    teamPlayersComponent.prototype.formatLogoSource = function (player) {
        if (player == null) {
            return "../assets/img/general/silhouette.png";
        }
        else {
            return "../assets/img/players/" + player.playerID + ".jpg";
        }
    };
    teamPlayersComponent.prototype.filterRemove = function (player) {
    };
    teamPlayersComponent.prototype.getPointsForPlayer = function (player) {
        var points = 0;
        switch (player.position) {
            case 'Goalkeeper':
                if (this.team.goalkeeperPoints != undefined && this.team.goalkeeperPoints > 0) {
                    points = this.team.goalkeeperPoints;
                }
                break;
            case 'Forward':
                if (this.team.forwardPoints != undefined && this.team.forwardPoints > 0) {
                    points = this.team.forwardPoints;
                }
                break;
            case 'Defender':
                if (this.team.defenderPoints != undefined && this.team.defenderPoints > 0) {
                    points = this.team.defenderPoints;
                }
                break;
            case 'Midfielder':
                if (this.team.midfielderPoints != undefined && this.team.midfielderPoints > 0) {
                    points = this.team.midfielderPoints;
                }
                break;
            case 'Flex':
                if (this.team.flexPoints != undefined && this.team.flexPoints > 0) {
                    points = this.team.flexPoints;
                }
                break;
        }
        return this.round(points, 1).toFixed(1);
    };
    teamPlayersComponent.prototype.submitUserTeam = function () {
    };
    /** Service calls **/
    /**
     * getUserTeamWithId - Get the UserTeam data for this given Contest Context
     * @param id - viEmKKJgFh UserTeam example
     * @returns UserTeam
     */
    teamPlayersComponent.prototype.getUserTeamWithId = function (id) {
        this.userTeamService.getUserTeamsWithID(id);
    };
    /**
     * getPlayerInfoForTeam - Just get players info for present team in contest
     * - Issues with the getPLayerInfoForUserTeam() at the mo, fix when subscription is fixed
     * @param team
     */
    teamPlayersComponent.prototype.getPlayerInfoForTeam = function (team) {
        this.playersInUserTeam = [];
        this.playerService.getPlayersForUserTeam(team);
    };
    /**
     * getAllPlayers - Use to get and store all players at once (will drop later)
     * - Use the players[playerID], playerID from UserTeam.defender(PlayerEvent).playerID
     */
    teamPlayersComponent.prototype.getAllPlayers = function () {
        this.playerService.getPlayers();
    };
    __decorate([
        core_1.Input()
    ], teamPlayersComponent.prototype, "edit");
    __decorate([
        core_1.Input()
    ], teamPlayersComponent.prototype, "live");
    __decorate([
        core_1.Input()
    ], teamPlayersComponent.prototype, "mode");
    __decorate([
        core_1.Input()
    ], teamPlayersComponent.prototype, "typeId");
    __decorate([
        core_1.Input()
    ], teamPlayersComponent.prototype, "open");
    __decorate([
        core_1.Input()
    ], teamPlayersComponent.prototype, "submit_disabled");
    teamPlayersComponent = __decorate([
        core_1.Component({
            selector: 'team-players',
            templateUrl: 'teamplayers.template.html',
            styleUrls: ['teamplayers.scss'],
            directives: [common_1.NgFor],
            providers: [playerService_1.PlayerService, UserTeamService_1.UserTeamService]
        })
    ], teamPlayersComponent);
    return teamPlayersComponent;
})(baseComponent_1.baseComponent);
exports.teamPlayersComponent = teamPlayersComponent;
//# sourceMappingURL=team.players.component.js.map