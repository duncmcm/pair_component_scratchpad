/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { NgFor } from '@angular/common';
import {UserTeam} from "../../../../models/userteam";
import {Player} from "../../../../models/player";
import {PlayerService} from "../../../../services/playerService";
import {baseComponent} from "../../../base/baseComponent";
import {UserTeamService} from "../../../../services/UserTeamService";

@Component({
    selector: 'team-players',
    templateUrl: 'teamplayers.template.html',
    styleUrls: ['teamplayers.scss'],
    directives: [NgFor],
    providers: [PlayerService, UserTeamService]
})

export class teamPlayersComponent extends baseComponent {

    @Input() edit:Boolean;
    @Input() live:Boolean;
    @Input() mode:string;
    @Input() typeId:number;

    public players:Player[] = [];
    public playersInUserTeam:Player[] = [];
    public playerService:PlayerService;
    public userTeamService:UserTeamService;
    public team:UserTeam;
    public teamPoints:string;
    public getTeams:Array<string> = [];

    @Input() open:Boolean = false;
    @Input() submit_disabled:Boolean = true;

    constructor(service:PlayerService, userService:UserTeamService, ref: ChangeDetectorRef) {
        super();
        var that = this;
        this.playerService = service;
        this.userTeamService = userService;

        this.subscription1 = this.playerService.playerAnnounce.subscribe(
            player => {
                this.playersInUserTeam.push(player);
                console.log('player received in Team Player Panel ' +player.lastName);

                this.processPlayerInfo();
                ref.detectChanges();
            });

        this.subscription3 = this.playerService.allPlayerAnnounce.subscribe(
            status => {
                //this.players.push(player);
                console.log('All Players received ' +status);
                ref.detectChanges();
            });

        this.subscription2 = this.userTeamService.teamAnnounce.subscribe(
            team => {
                this.team = team;
                console.log('team received in Team Player Panel ' +team.name);
                ref.detectChanges();

                this.getPlayerInfoForTeam(this.team);
            });

        //setInterval(function(){
        //    that.getPlayerInfoForTeam(that.team);
        //}, 5000);
    }

    ngOnInit() {
        super.ngOnInit();
        this.getUserTeamWithId('xx32YGtlxm');
    }

    /** Player Info Business Login **/

    processPlayerInfo() {
        if (this.playersInUserTeam.length != 5) {
            this.submit_disabled = true;
            return;
        }
        this.submit_disabled = false;
        this.formatTeamPoints();
    }


    formatTeamPoints() {
        this.teamPoints = this.round(this.team.points, 1).toFixed(1);
        console.log(' team points = ' +this.teamPoints);
    }

    round(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }

    formatLogoSource(player:Player) {
        if (player == null) {
            return "../assets/img/general/silhouette.png";
        } else {
            return "../assets/img/players/" +player.playerID +".jpg";
        }
    }

    filterRemove(player:Player) {

    }

    getPointsForPlayer(player:Player) {
        let points:number = 0;
        switch (player.position) {
            case 'Goalkeeper':
                if (this.team.goalkeeperPoints != undefined && this.team.goalkeeperPoints > 0) {
                    points = this.team.goalkeeperPoints;
                }
                break;
            case 'Forward':
                if (this.team.forwardPoints != undefined && this.team.forwardPoints > 0) {
                    points = this.team.forwardPoints;
                }
                break;
            case 'Defender':
                if (this.team.defenderPoints != undefined && this.team.defenderPoints > 0) {
                    points = this.team.defenderPoints;
                }
                break;
            case 'Midfielder':
                if (this.team.midfielderPoints != undefined && this.team.midfielderPoints > 0) {
                    points = this.team.midfielderPoints;
                }
                break;
            case 'Flex':
                if (this.team.flexPoints != undefined && this.team.flexPoints > 0) {
                    points = this.team.flexPoints;
                }
                break;
        }
        return this.round(points, 1).toFixed(1);
    }

    submitUserTeam() {

    }


    /** Service calls **/

    /**
     * getUserTeamWithId - Get the UserTeam data for this given Contest Context
     * @param id - viEmKKJgFh UserTeam example
     * @returns UserTeam
     */
    getUserTeamWithId(id:string) {
        this.userTeamService.getUserTeamsWithID(id);
    }

    /**
     * getPlayerInfoForTeam - Just get players info for present team in contest
     * - Issues with the getPLayerInfoForUserTeam() at the mo, fix when subscription is fixed
     * @param team
     */
    getPlayerInfoForTeam(team:UserTeam) {
        this.playersInUserTeam = [];
        this.playerService.getPlayersForUserTeam(team)
    }

    /**
     * getAllPlayers - Use to get and store all players at once (will drop later)
     * - Use the players[playerID], playerID from UserTeam.defender(PlayerEvent).playerID
     */
    getAllPlayers() {
        this.playerService.getPlayers();
    }
}