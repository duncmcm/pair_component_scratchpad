var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 15/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var topPanelComponent = (function () {
    function topPanelComponent(route, router) {
        this.route = route;
        this.router = router;
    }
    topPanelComponent.prototype.ngOnInit = function () {
    };
    topPanelComponent.prototype.showRules = function () {
        this.router.navigate(['rules'], { relativeTo: this.route });
    };
    __decorate([
        core_1.Input()
    ], topPanelComponent.prototype, "edit");
    __decorate([
        core_1.Input()
    ], topPanelComponent.prototype, "live");
    __decorate([
        core_1.Input()
    ], topPanelComponent.prototype, "mode");
    __decorate([
        core_1.Input()
    ], topPanelComponent.prototype, "typeId");
    topPanelComponent = __decorate([
        core_1.Component({
            selector: 'top-panel',
            templateUrl: 'toppanel.template.html',
            directives: [common_1.NgFor]
        })
    ], topPanelComponent);
    return topPanelComponent;
})();
exports.topPanelComponent = topPanelComponent;
//# sourceMappingURL=toppanel.component.js.map