/**
 * Created by duncmcm on 15/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'top-panel',
    templateUrl: 'toppanel.template.html',
    directives: [NgFor],
})

export class topPanelComponent implements OnInit {

    @Input() edit:Boolean;
    @Input() live:Boolean;
    @Input() mode:string;
    @Input() typeId:number;

    constructor(private route: ActivatedRoute, private router: Router) {

    }

    ngOnInit() {

    }

    showRules() {
        this.router.navigate(['rules'], { relativeTo: this.route });
    }
}