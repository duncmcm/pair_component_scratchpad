var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 20/12/2016.
 */
var core_1 = require('@angular/core');
var gamingComponent = (function () {
    function gamingComponent() {
    }
    gamingComponent.prototype.ngOnInit = function () {
    };
    gamingComponent = __decorate([
        core_1.Component({
            selector: 'gaming-panel',
            templateUrl: 'gaming.template.html'
        })
    ], gamingComponent);
    return gamingComponent;
})();
exports.gamingComponent = gamingComponent;
//# sourceMappingURL=gaming.component.js.map