var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 02/12/2016.
 */
var core_1 = require('@angular/core');
var userService_1 = require("../../services/userService");
var headerComponent = (function () {
    function headerComponent(userService) {
        this.onLoggedOut = new core_1.EventEmitter();
        this.onDisplayMyContests = new core_1.EventEmitter();
        this._users = [];
        this.userService = userService;
    }
    Object.defineProperty(headerComponent.prototype, "users", {
        set: function (value) {
            if (value) {
                this._users = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    headerComponent.prototype.ngOnInit = function () {
    };
    headerComponent.prototype.refreshUsers = function () {
        this.userService.getUsers(); //.subscribe((users) => this.users = users );
    };
    headerComponent.prototype.logout = function () {
        this.onLoggedOut.emit(false);
    };
    headerComponent.prototype.displayMyContests = function () {
        this.onDisplayMyContests.emit(true);
    };
    __decorate([
        core_1.Output()
    ], headerComponent.prototype, "onLoggedOut");
    __decorate([
        core_1.Output()
    ], headerComponent.prototype, "onDisplayMyContests");
    headerComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            providers: [userService_1.UserService],
            templateUrl: 'header.template.html',
            styleUrls: ['header.scss']
        })
    ], headerComponent);
    return headerComponent;
})();
exports.headerComponent = headerComponent;
//# sourceMappingURL=header.component.js.map