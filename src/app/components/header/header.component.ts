/**
 * Created by duncmcm on 02/12/2016.
 */
import {Component, OnInit, EventEmitter, Output } from '@angular/core';
import {UserService} from "../../services/userService";
import {UserItem} from "../../models/user";
import {UserBalance} from "../../models/user_balance";

@Component({
    selector: 'app-header',
    providers: [UserService],
    templateUrl: 'header.template.html',
    styleUrls: ['header.scss']
})

export class headerComponent implements OnInit {

    public userService:UserService;
    public balance:number;
    @Output() onLoggedOut = new EventEmitter<boolean>();
    @Output() onDisplayMyContests = new EventEmitter<boolean>();

    constructor(userService:UserService) {
        this.userService = userService;
    }

    private _users:UserItem[] = [];
    set users(value) {
        if (value) {
            this._users = value;
        }
    }

    ngOnInit() {

    }

    refreshUsers() {
        this.userService.getUsers(); //.subscribe((users) => this.users = users );
    }

    logout() {
        this.onLoggedOut.emit(false);
    }

    displayMyContests() {
        this.onDisplayMyContests.emit(true);
    }
}