var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 07/12/2016.
 */
var core_1 = require('@angular/core');
var user_1 = require("../../models/user");
var _ = require('underscore');
var loginComponent = (function () {
    function loginComponent(userService, router) {
        var _this = this;
        this.router = router;
        this.loggedInUser = new user_1.UserItem();
        this.users = [];
        this.invalidUser = false;
        this.submitted = false;
        this.userService = userService;
        this.userService.userConfirmed.subscribe(function (user) {
            _this.users.push(user);
            console.log("user received in Login component " + user.username);
        });
    }
    Object.defineProperty(loginComponent.prototype, "diagnostic", {
        get: function () { return JSON.stringify(this.loggedInUser); },
        enumerable: true,
        configurable: true
    });
    loginComponent.prototype.ngOnInit = function () {
        this.users = [];
        this.userService.getUsers();
    };
    loginComponent.prototype.onSubmit = function () {
        if (!this.checkForValidUser(this.loggedInUser)) {
            this.invalidUser = true;
            return;
        }
        else {
            this.invalidUser = false;
        }
        this.router.navigate(["contests"]);
        this.submitted = true;
        console.log("User Logged In " + JSON.stringify(this.loggedInUser));
    };
    loginComponent.prototype.checkForValidUser = function (loggedInUser) {
        var validUser = _.find(this.users, function (user) {
            return loggedInUser.username == user.username;
        });
        if (validUser) {
            this.loggedInUser = validUser;
            this.userService.loggedInUser = this.loggedInUser;
            return true;
        }
        else {
            return false;
        }
    };
    loginComponent = __decorate([
        core_1.Component({
            selector: 'login-panel',
            //providers: [UserService],
            templateUrl: 'login.template.html',
            styleUrls: ['login.scss']
        })
    ], loginComponent);
    return loginComponent;
})();
exports.loginComponent = loginComponent;
//# sourceMappingURL=login.component.js.map