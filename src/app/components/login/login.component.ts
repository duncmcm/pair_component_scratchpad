/**
 * Created by duncmcm on 07/12/2016.
 */
import {Component, OnInit, Output, EventEmitter } from '@angular/core';
import {UserService} from "../../services/userService";
import { Router, ActivatedRoute } from '@angular/router';
import {UserItem} from "../../models/user";
import * as _ from 'underscore';

@Component({
    selector: 'login-panel',
    //providers: [UserService],
    templateUrl: 'login.template.html',
    styleUrls:['login.scss']
})

export class loginComponent implements OnInit {

    public userService:UserService;
    public loggedInUser:UserItem = new UserItem();
    public users:UserItem[] = [];
    public invalidUser:Boolean = false;
    public submitted:Boolean = false;

    constructor(userService:UserService, private router: Router) {
        this.userService = userService;

        this.userService.userConfirmed.subscribe(
            user => {
                this.users.push(user);
                console.log("user received in Login component " +user.username);
            });
    }

    get diagnostic() { return JSON.stringify(this.loggedInUser); }

    ngOnInit() {
        this.users = [];
        this.userService.getUsers();
    }

    onSubmit() {
        if(!this.checkForValidUser(this.loggedInUser)) {
            this.invalidUser = true;
            return;
        } else {
            this.invalidUser = false;
        }

        this.router.navigate(["contests"]);
        this.submitted = true;
        console.log("User Logged In " +JSON.stringify(this.loggedInUser));
    }

    checkForValidUser(loggedInUser:UserItem) {
        let validUser = _.find(this.users, function(user){
            return loggedInUser.username == user.username;
        });
        if (validUser) {
            this.loggedInUser =  validUser;
            this.userService.loggedInUser = this.loggedInUser;
            return true;
        } else {
            return false;
        }
    }
}