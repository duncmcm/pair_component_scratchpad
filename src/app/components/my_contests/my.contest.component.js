var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 07/12/2016.
 */
var core_1 = require('@angular/core');
var myContestComponent = (function () {
    function myContestComponent(route, router, service) {
        this.route = route;
        this.router = router;
        /**
         * accessBetslip - Set to true for mobile
         * @type {boolean}
         */
        this.accessBetslip = false;
        this.contestService = service;
    }
    myContestComponent.prototype.ngOnInit = function () {
    };
    /**
     * Send a contest ID for game page - temp contest Its Unbelievable Jeff
     */
    myContestComponent.prototype.selectContest = function () {
        this.contestService.getContestWithId('Oempv5lGD6');
    };
    myContestComponent.prototype.onSelect = function (relativeRoute, event) {
        if (relativeRoute == "") {
            this.router.navigate(['contests/mycontests']);
        }
        else {
            this.router.navigate([relativeRoute], { relativeTo: this.route });
        }
        this.styleTags(event);
    };
    myContestComponent.prototype.styleTags = function (event) {
        var parent = event.target.parentElement.parentElement;
        var allButtons = parent.getElementsByClassName('btn');
        for (var t = 0; t < allButtons.length; t++) {
            var button = allButtons[t];
            if (button.classList.contains('active')) {
                button.classList.remove(['active']);
            }
        }
        event.target.classList.add('active');
    };
    myContestComponent = __decorate([
        core_1.Component({
            selector: 'my-contest-panel',
            templateUrl: 'my.contests.template.html',
            directives: []
        })
    ], myContestComponent);
    return myContestComponent;
})();
exports.myContestComponent = myContestComponent;
//# sourceMappingURL=my.contest.component.js.map