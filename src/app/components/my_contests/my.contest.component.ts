/**
 * Created by duncmcm on 07/12/2016.
 */
import {Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Contest} from "../../models/contest";
import {ContestService} from "../../services/contestService";
import { Observable, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-contest-panel',
    templateUrl: 'my.contests.template.html',
    directives: [],
})

export class myContestComponent implements OnInit {

    /**
     * accessBetslip - Set to true for mobile
     * @type {boolean}
     */
    public accessBetslip:Boolean = false;

    public contestService:ContestService;
    public contest:Contest;
    public subscription:Subscription;

    constructor(private route: ActivatedRoute, private router: Router, service:ContestService) {
        this.contestService = service;
    }

    ngOnInit() {

    }

    /**
     * Send a contest ID for game page - temp contest Its Unbelievable Jeff
     */
    selectContest() {
        this.contestService.getContestWithId('Oempv5lGD6');
    }

    onSelect(relativeRoute:string, event) { //'', live, history
        if (relativeRoute == "") {
            this.router.navigate(['contests/mycontests']);
        } else {
            this.router.navigate([relativeRoute], { relativeTo: this.route });
        }
        this.styleTags(event);
    }

    styleTags(event:any) {
        let parent = event.target.parentElement.parentElement;
        let allButtons:any = parent.getElementsByClassName('btn');
        for (var t=0; t < allButtons.length; t++) {
            let button = allButtons[t];
            if (button.classList.contains('active')) { // Assume only one class
                button.classList.remove(['active']);
            }
        }
        event.target.classList.add('active');
    }

}