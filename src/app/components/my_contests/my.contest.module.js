var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_1 = require('@angular/router');
var my_contest_component_1 = require('./my.contest.component');
var my_contests_upcoming_component_1 = require('./subcomponents/upcoming/my.contests.upcoming.component');
var my_contests_live_component_1 = require('./subcomponents/live/my.contests.live.component');
var my_contests_closed_component_1 = require('./subcomponents/closed/my.contests.closed.component');
var shared_module_1 = require('../../components/shared/shared.module');
var game_module_1 = require('../game/game.module');
var myContestsRoutes = [
    {
        path: 'contests/mycontests',
        component: my_contest_component_1.myContestComponent,
        children: [
            { path: '', component: my_contests_upcoming_component_1.myContestsUpcomingComponent },
            { path: 'open', component: my_contests_upcoming_component_1.myContestsUpcomingComponent },
            { path: 'live', component: my_contests_live_component_1.myContestsLiveComponent },
            { path: 'closed', component: my_contests_closed_component_1.myContestsClosedComponent },
        ]
    }
];
var MyContestsModule = (function () {
    function MyContestsModule() {
    }
    MyContestsModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(myContestsRoutes),
                platform_browser_1.BrowserModule,
                shared_module_1.SharedModule,
                game_module_1.GameModule
            ],
            exports: [
                router_1.RouterModule
            ],
            declarations: [
                my_contest_component_1.myContestComponent,
                my_contests_upcoming_component_1.myContestsUpcomingComponent,
                my_contests_live_component_1.myContestsLiveComponent,
                my_contests_closed_component_1.myContestsClosedComponent
            ]
        })
    ], MyContestsModule);
    return MyContestsModule;
})();
exports.MyContestsModule = MyContestsModule;
//# sourceMappingURL=my.contest.module.js.map