/**
 * Created by duncmcm on 28/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { myContestComponent } from './my.contest.component';
import { myContestsUpcomingComponent } from './subcomponents/upcoming/my.contests.upcoming.component';
import { myContestsLiveComponent } from './subcomponents/live/my.contests.live.component';
import { myContestsClosedComponent } from './subcomponents/closed/my.contests.closed.component';
import { SharedModule } from '../../components/shared/shared.module';
import { GameModule } from '../game/game.module';

const myContestsRoutes: Routes = [
    {
        path: 'contests/mycontests',
        component: myContestComponent,
        children: [
            { path: '', component: myContestsUpcomingComponent},
            { path: 'open', component: myContestsUpcomingComponent},
            { path: 'live', component: myContestsLiveComponent},
            { path: 'closed', component: myContestsClosedComponent},
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(myContestsRoutes),
        BrowserModule,
        SharedModule,
        GameModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        myContestComponent,
        myContestsUpcomingComponent,
        myContestsLiveComponent,
        myContestsClosedComponent
    ]
})
export class MyContestsModule { }