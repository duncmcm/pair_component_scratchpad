var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 28/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var _ = require('underscore');
var myContestsClosedComponent = (function () {
    function myContestsClosedComponent(route, router, contestService) {
        this.route = route;
        this.router = router;
        this.closedContests = [];
        this.contestService = contestService;
    }
    myContestsClosedComponent.prototype.ngOnInit = function () {
        this.closedContests = this.filterContests(this.contestService.contests);
    };
    myContestsClosedComponent.prototype.filterContests = function (list) {
        var filteredList = _.filter(list, function (contest) {
            return contest.status == 'Closed';
        });
        return filteredList;
    };
    myContestsClosedComponent.prototype.showContest = function (contest) {
        console.log('Showing Contest ' + contest.name);
        this.router.navigate(['contest/closed']);
        this.contestService.selectedContest = contest;
        //this.contestService.getContestWithId(contest.id);
    };
    myContestsClosedComponent = __decorate([
        core_1.Component({
            selector: 'my-contests-closed',
            templateUrl: 'my.contests.closed.template.html',
            directives: [common_1.NgFor]
        })
    ], myContestsClosedComponent);
    return myContestsClosedComponent;
})();
exports.myContestsClosedComponent = myContestsClosedComponent;
//# sourceMappingURL=my.contests.closed.component.js.map