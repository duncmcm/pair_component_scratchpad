/**
 * Created by duncmcm on 28/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import {ContestService} from "../../../../services/contestService";
import {Contest} from "../../../../models/contest";
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-contests-closed',
    templateUrl: 'my.contests.closed.template.html',
    directives: [NgFor],
})

export class myContestsClosedComponent implements OnInit {

    public closedContests:Contest[] = [];
    public contestService:ContestService;

    constructor(private route: ActivatedRoute, private router: Router, contestService:ContestService) {
        this.contestService = contestService;
    }

    ngOnInit() {
        this.closedContests = this.filterContests(this.contestService.contests);
    }

    filterContests(list:Contest[]) {
        let filteredList:Contest[] = _.filter(list, function(contest) {
            return contest.status == 'Closed';
        });
        return filteredList;
    }

    showContest(contest:Contest) {
        console.log('Showing Contest ' +contest.name);
        this.router.navigate(['contest/closed']);
        this.contestService.selectedContest = contest;
        //this.contestService.getContestWithId(contest.id);
    }
}