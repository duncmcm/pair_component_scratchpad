var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 28/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var _ = require('underscore');
var myContestsLiveComponent = (function () {
    function myContestsLiveComponent(contestService) {
        this.liveContests = [];
        this.contestService = contestService;
    }
    myContestsLiveComponent.prototype.ngOnInit = function () {
        this.liveContests = this.filterContests(this.contestService.contests);
    };
    myContestsLiveComponent.prototype.filterContests = function (list) {
        var filteredList = _.filter(list, function (contest) {
            return contest.status == 'Live';
        });
        return filteredList;
    };
    myContestsLiveComponent.prototype.enterContest = function (contest) {
        console.log('Enetring COntest ' + contest.name);
    };
    myContestsLiveComponent = __decorate([
        core_1.Component({
            selector: 'my-contests-live',
            templateUrl: 'my.contests.live.template.html',
            directives: [common_1.NgFor]
        })
    ], myContestsLiveComponent);
    return myContestsLiveComponent;
})();
exports.myContestsLiveComponent = myContestsLiveComponent;
//# sourceMappingURL=my.contests.live.component.js.map