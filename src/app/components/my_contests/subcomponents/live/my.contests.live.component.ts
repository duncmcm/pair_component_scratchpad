/**
 * Created by duncmcm on 28/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import * as _ from 'underscore';
import {Contest} from "../../../../models/contest";
import {ContestService} from "../../../../services/contestService";

@Component({
    selector: 'my-contests-live',
    templateUrl: 'my.contests.live.template.html',
    directives: [NgFor],
})

export class myContestsLiveComponent implements OnInit {

    public liveContests:Contest[] = [];
    public contestService:ContestService;

    constructor(contestService:ContestService) {
        this.contestService = contestService;
    }

    ngOnInit() {
        this.liveContests = this.filterContests(this.contestService.contests);
    }

    filterContests(list:Contest[]) {
        let filteredList:Contest[] = _.filter(list, function(contest) {
            return contest.status == 'Live';
        });
        return filteredList;
    }

    enterContest(contest:Contest) {
        console.log('Enetring COntest ' +contest.name);
    }
}