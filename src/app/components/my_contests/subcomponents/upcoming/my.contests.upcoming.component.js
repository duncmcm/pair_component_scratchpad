var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 28/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var _ = require('underscore');
var myContestsUpcomingComponent = (function () {
    function myContestsUpcomingComponent(contestService, route, router) {
        this.route = route;
        this.router = router;
        this.openContests = [];
        this.contestService = contestService;
    }
    myContestsUpcomingComponent.prototype.ngOnInit = function () {
        this.openContests = this.filterContests(this.contestService.contests);
    };
    myContestsUpcomingComponent.prototype.filterContests = function (list) {
        var filteredList = _.filter(list, function (contest) {
            return contest.status == 'Open';
        });
        return filteredList;
    };
    myContestsUpcomingComponent.prototype.enterContest = function (contest) {
        console.log('Entering Contest ' + contest.name);
        this.contestService.selectedContest = contest;
        this.router.navigate(['contest']);
    };
    myContestsUpcomingComponent = __decorate([
        core_1.Component({
            selector: 'my-contests-upcoming',
            templateUrl: 'my.contests.upcoming.template.html',
            directives: [common_1.NgFor]
        })
    ], myContestsUpcomingComponent);
    return myContestsUpcomingComponent;
})();
exports.myContestsUpcomingComponent = myContestsUpcomingComponent;
//# sourceMappingURL=my.contests.upcoming.component.js.map