/**
 * Created by duncmcm on 28/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import {Contest} from "../../../../models/contest";
import {ContestService} from "../../../../services/contestService";
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-contests-upcoming',
    templateUrl: 'my.contests.upcoming.template.html',
    directives: [NgFor],
})

export class myContestsUpcomingComponent implements OnInit {

    public openContests:Contest[] = [];
    public contestService:ContestService;

    constructor(contestService:ContestService, private route: ActivatedRoute, private router: Router) {
        this.contestService = contestService;
    }

    ngOnInit() {
        this.openContests = this.filterContests(this.contestService.contests);
    }

    filterContests(list:Contest[]) {
        let filteredList:Contest[] = _.filter(list, function(contest) {
            return contest.status == 'Open';
        });
        return filteredList;
    }

    enterContest(contest:Contest) {
        console.log('Entering Contest ' +contest.name);
        this.contestService.selectedContest = contest;
        this.router.navigate(['contest']);
    }
}