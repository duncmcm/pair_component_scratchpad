var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 20/12/2016.
 */
var core_1 = require('@angular/core');
var rulesComponent = (function () {
    function rulesComponent() {
    }
    rulesComponent.prototype.ngOnInit = function () {
    };
    rulesComponent = __decorate([
        core_1.Component({
            selector: 'rules-panel',
            templateUrl: 'rules.template.html'
        })
    ], rulesComponent);
    return rulesComponent;
})();
exports.rulesComponent = rulesComponent;
//# sourceMappingURL=rules.component.js.map