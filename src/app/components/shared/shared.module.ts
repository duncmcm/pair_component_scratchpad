/**
 * Created by duncmcm on 28/12/2016.
 */
import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { bettingSlipComponent } from "../bettingslip/betting.slip.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        bettingSlipComponent
    ],
    providers: [
    ],
    exports: [
        bettingSlipComponent
    ]
})
export class SharedModule {}