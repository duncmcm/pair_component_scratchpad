var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 13/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var fixtureComponent = (function () {
    function fixtureComponent() {
    }
    fixtureComponent.prototype.ngOnInit = function () {
        console.log('fixture init');
    };
    __decorate([
        core_1.Input()
    ], fixtureComponent.prototype, "fixture");
    __decorate([
        core_1.Input()
    ], fixtureComponent.prototype, "date");
    fixtureComponent = __decorate([
        core_1.Component({
            selector: 'fixture',
            templateUrl: 'fixture.template.html',
            styleUrls: ['fixture.scss'],
            directives: [common_1.NgFor]
        })
    ], fixtureComponent);
    return fixtureComponent;
})();
exports.fixtureComponent = fixtureComponent;
//# sourceMappingURL=fixture.component.js.map