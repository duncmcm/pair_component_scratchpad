/**
 * Created by duncmcm on 13/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { Fixture } from '../../../../models/fixture';

@Component({
    selector: 'fixture',
    templateUrl: 'fixture.template.html',
    styleUrls: ['fixture.scss'],
    directives: [NgFor],
})

export class fixtureComponent implements OnInit {

    @Input() fixture:Fixture;
    @Input() date:string;

    constructor() {
    }

    ngOnInit() {
        console.log('fixture init');
    }
}