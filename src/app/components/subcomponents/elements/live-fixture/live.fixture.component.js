var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 19/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var liveFixtureComponent = (function () {
    function liveFixtureComponent() {
    }
    liveFixtureComponent.prototype.ngOnInit = function () {
        if (this.fixture.teamScoreHome == undefined) {
            this.fixture.teamScoreHome = 0;
        }
        if (this.fixture.teamScoreAway == undefined) {
            this.fixture.teamScoreAway = 0;
        }
    };
    __decorate([
        core_1.Input()
    ], liveFixtureComponent.prototype, "fixture");
    liveFixtureComponent = __decorate([
        core_1.Component({
            selector: 'live-fixture',
            templateUrl: 'live.fixture.template.html',
            styleUrls: ['live.fixture.scss'],
            directives: [common_1.NgFor]
        })
    ], liveFixtureComponent);
    return liveFixtureComponent;
})();
exports.liveFixtureComponent = liveFixtureComponent;
//# sourceMappingURL=live.fixture.component.js.map