/**
 * Created by duncmcm on 19/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import {Fixture} from "../../../../models/fixture";

@Component({
    selector: 'live-fixture',
    templateUrl: 'live.fixture.template.html',
    styleUrls: ['live.fixture.scss'],
    directives: [NgFor],
})

export class liveFixtureComponent implements OnInit {

    @Input() fixture:Fixture;

    constructor() {

    }

    ngOnInit() {
        if (this.fixture.teamScoreHome == undefined) {
            this.fixture.teamScoreHome = 0;
        }
        if (this.fixture.teamScoreAway == undefined) {
            this.fixture.teamScoreAway = 0;
        }
    }
}