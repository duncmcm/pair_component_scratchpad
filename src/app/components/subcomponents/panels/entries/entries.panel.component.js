var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 13/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var UserTeamService_1 = require('../../../../services/UserTeamService');
var entriesPanelComponent = (function () {
    function entriesPanelComponent(service, ref) {
        var _this = this;
        this.teams = [];
        this.teamService = service;
        this.subscription = this.teamService.teamAnnounce.subscribe(function (team) {
            _this.teams.push(team);
            console.log('team received in Panel ' + team.name);
            ref.detectChanges();
        });
    }
    entriesPanelComponent.prototype.ngOnInit = function () {
    };
    entriesPanelComponent.prototype.ngOnChanges = function () {
        if (this.selectedContestID == null || this.selectedContestID != this.contest.id) {
            for (var i = 0; i < this.contest.fixtures.length; i++) {
                this.teamService.getUserTeamsWithID(this.contest.entries[i]);
            }
        }
        this.selectedContestID = this.contest.id;
    };
    entriesPanelComponent.prototype.ngOnDestroy = function () {
        // prevent memory leak when component destroyed
        this.subscription.unsubscribe();
    };
    __decorate([
        core_1.Input()
    ], entriesPanelComponent.prototype, "contest");
    entriesPanelComponent = __decorate([
        core_1.Component({
            selector: 'entries-panel',
            templateUrl: 'entries.panel.template.html',
            styleUrls: ['entries.panel.scss'],
            directives: [common_1.NgFor],
            providers: [UserTeamService_1.UserTeamService]
        })
    ], entriesPanelComponent);
    return entriesPanelComponent;
})();
exports.entriesPanelComponent = entriesPanelComponent;
//# sourceMappingURL=entries.panel.component.js.map