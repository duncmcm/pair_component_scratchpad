/**
 * Created by duncmcm on 13/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, ChangeDetectorRef } from '@angular/core';
import { NgFor } from '@angular/common';
import { UserTeam } from '../../../../models/userteam';
import { fixtureComponent } from '../../elements/fixture/fixture.component';
import { Observable, Subscription } from 'rxjs';
import { UserTeamService } from '../../../../services/UserTeamService';
import { Contest } from '../../../../models/contest';

@Component({
    selector: 'entries-panel',
    templateUrl: 'entries.panel.template.html',
    styleUrls: ['entries.panel.scss'],
    directives: [NgFor],
    providers:[UserTeamService]
})

export class entriesPanelComponent implements OnInit {

    @Input() contest:Contest;

    public teamService:UserTeamService;
    public subscription:Subscription;
    public teams:UserTeam[] = [];
    public selectedContestID:number;

    constructor(service:UserTeamService, ref:ChangeDetectorRef) {
        this.teamService = service;

        this.subscription = this.teamService.teamAnnounce.subscribe(
            team => {
                this.teams.push(team);
                console.log('team received in Panel ' +team.name);
                ref.detectChanges();
            });
    }

    ngOnInit() {

    }

    ngOnChanges() {
        if (this.selectedContestID == null || this.selectedContestID != this.contest.id) {
            for (var i=0; i < this.contest.fixtures.length; i++) {
                this.teamService.getUserTeamsWithID(this.contest.entries[i]);
            }
        }
        this.selectedContestID = this.contest.id;
    }

    ngOnDestroy() {
        // prevent memory leak when component destroyed
        this.subscription.unsubscribe();
    }


}