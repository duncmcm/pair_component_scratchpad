var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 13/12/2016.
 */
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var fixtureService_1 = require('../../../../services/fixtureService');
var fixture_component_1 = require('../../elements/fixture/fixture.component');
var fixturesPanelComponent = (function () {
    function fixturesPanelComponent(service, ref) {
        var _this = this;
        this.fixtures = [];
        this.fixtureService = service;
        this.subscription = this.fixtureService.fixtureAnnounce.subscribe(function (fixture) {
            _this.fixtures.push(fixture);
            console.log('fixture received in Panel ' + fixture.teamNameHome);
            ref.detectChanges();
        });
    }
    fixturesPanelComponent.prototype.ngOnChanges = function () {
        if (this.selectedContestID == null || this.selectedContestID != this.contest.id) {
            for (var i = 0; i < this.contest.fixtures.length; i++) {
                this.fixtureService.getFixtureWithID(this.contest.fixtures[i]);
            }
        }
        this.date = this.contest.startDateDayFormat;
        this.selectedContestID = this.contest.id;
    };
    fixturesPanelComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input()
    ], fixturesPanelComponent.prototype, "contest");
    fixturesPanelComponent = __decorate([
        core_1.Component({
            selector: 'fixtures-panel',
            templateUrl: 'fixtures.panel.template.html',
            styleUrls: ['fixtures.panel.scss'],
            directives: [common_1.NgFor, fixture_component_1.fixtureComponent],
            providers: [fixtureService_1.FixtureService]
        })
    ], fixturesPanelComponent);
    return fixturesPanelComponent;
})();
exports.fixturesPanelComponent = fixturesPanelComponent;
//# sourceMappingURL=fixtures.panel.component.js.map