/**
 * Created by duncmcm on 13/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input, OnChanges, ChangeDetectorRef } from '@angular/core';
import { NgFor } from '@angular/common';
import { Contest } from '../../../../models/contest';
import { FixtureService } from '../../../../services/fixtureService';
import {Fixture} from "../../../../models/fixture";
import { fixtureComponent } from '../../elements/fixture/fixture.component';
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'fixtures-panel',
    templateUrl: 'fixtures.panel.template.html',
    styleUrls: ['fixtures.panel.scss'],
    directives: [NgFor, fixtureComponent],
    providers: [FixtureService]
})

export class fixturesPanelComponent implements OnInit, OnChanges {

    @Input() contest:Contest;
    public fixtureService:FixtureService;
    public fixtures:Fixture[] = [];
    public date:string;
    public subscription:Subscription;

    public selectedContestID:number;

    constructor(service:FixtureService, ref: ChangeDetectorRef) {
        this.fixtureService = service;

        this.subscription = this.fixtureService.fixtureAnnounce.subscribe(
            fixture => {
                this.fixtures.push(fixture);
                console.log('fixture received in Panel ' +fixture.teamNameHome);
                ref.detectChanges();
            });
    }

    ngOnChanges() {
        if (this.selectedContestID == null || this.selectedContestID != this.contest.id) {
            for (var i=0; i < this.contest.fixtures.length; i++) {
                this.fixtureService.getFixtureWithID(this.contest.fixtures[i]);
            }
        }
        this.date = this.contest.startDateDayFormat;
        this.selectedContestID = this.contest.id;
    }

    ngOnInit() {

    }
}