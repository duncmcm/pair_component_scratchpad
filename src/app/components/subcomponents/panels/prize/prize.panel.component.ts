/**
 * Created by duncmcm on 13/12/2016.
 */
import {Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { Contest } from '../../../../models/contest';

@Component({
    selector: 'prize-panel',
    templateUrl: 'prize.panel.template.html',
    styleUrls: ['prize.panel.scss'],
    directives: [NgFor],
})

export class prizePanelComponent implements OnInit {

    @Input() contest:Contest;

    constructor() {
    }

    ngOnInit() {

    }

}