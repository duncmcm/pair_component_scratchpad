/**
 * Created by duncmcm on 20/12/2016.
 */
import {Component} from '@angular/core'
import { ROUTER_DIRECTIVES } from '@angular/router';


@Component({
    selector: 'home',

    templateUrl: 'home.template.html',

    directives: [ROUTER_DIRECTIVES]
})
export class Home {

}