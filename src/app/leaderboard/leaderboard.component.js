var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var core_1 = require('@angular/core');
var LeaderboardComponent = (function () {
    function LeaderboardComponent() {
        var _this = this;
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    LeaderboardComponent.prototype.onPage = function (event) {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
            console.log('paged!', event);
        }, 100);
    };
    LeaderboardComponent.prototype.onDummySelected = function (event) {
        var _this = this;
        this.fetch(function (data) {
            _this.rows = data;
        });
    };
    LeaderboardComponent.prototype.onLiveSelected = function (event) {
        this.rows = [
            { position: '1', name: 'Leons Team', points: '530' },
            { position: '2', name: 'Andys Team', points: '120' },
            { position: '3', name: 'Duncans Team', points: '100' },
            { position: '4', name: 'Denises Team', points: '54' },
        ];
    };
    LeaderboardComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "app/leaderboard/data/100k.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    LeaderboardComponent.prototype.ngOnInit = function () {
    };
    LeaderboardComponent = __decorate([
        core_1.Component({
            selector: 'app-leaderboard',
            templateUrl: './leaderboard.component.html',
            styleUrls: ['./leaderboard.component.css']
        })
    ], LeaderboardComponent);
    return LeaderboardComponent;
})();
exports.LeaderboardComponent = LeaderboardComponent;
//# sourceMappingURL=leaderboard.component.js.map