import { Component, OnInit } from '@angular/core';
import {Contest} from "../models/contest";

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  rows = [];
  timeout: any;
  contest:Contest;

  constructor() {
    this.fetch((data) => {
      this.rows = data;
    });
  }

  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  onDummySelected(event) {
    this.fetch((data) => {
      this.rows = data;
    });
  }

  onLiveSelected(event) {
    this.rows = [
      { position: '1', name: 'Leons Team', points: '530' },
      { position: '2', name: 'Andys Team', points: '120' },
      { position: '3', name: 'Duncans Team', points: '100' },
      { position: '4', name: 'Denises Team', points: '54' },
    ];
    this.setDummyContest();
  }

  setDummyContest() {
    if (this.contest != null) {
      this.contest = null;
      this.contest = new Contest();
    }
    this.contest.name = '£9 Top Half Wins';
    this.contest.prizes = [100, 50, 20];
    this.contest.entries = ['1234', '5678', '0987'];
    this.contest.entryFee = 50;
    this.contest.startDateTime = 'Sat 25 Feb 3.00PM';
  }

  fetch(cb) {
    let req = new XMLHttpRequest();
    req.open('GET', `app/leaderboard/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  ngOnInit() {
  }

}
