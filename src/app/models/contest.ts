/**
 * Created by duncmcm on 07/12/2016.
 */
export class Contest {

    public prizes:any;
    public fixtures:any;
    public name:string;
    public entryFee:number;
    public prizeType:string;
    public contestType:string;
    public startDate:string;
    public startDateLong:string;
    public startDateDayFormat:string;
    public startDateTime:string;
    public entries:any;
    public status:string;

    public id:any;

    constructor() {
    }
}
