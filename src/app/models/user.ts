/**
 * Created by raj on 12/20/2015.
 */
import {BettingSlip} from './bettingslip';

export class UserItem {

    public id:string;
    public username:string;
    public email:string;
    public password:string;
    public balance:number;
    public bettingSlip:BettingSlip;

    constructor() {

    }
}