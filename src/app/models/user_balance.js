/**
 * Created by duncmcm on 02/12/2016.
 */
var UserBalance = (function () {
    function UserBalance(id, amount, currency) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
    }
    return UserBalance;
})();
exports.UserBalance = UserBalance;
//# sourceMappingURL=user_balance.js.map