/**
 * Created by duncmcm on 02/12/2016.
 */
export class UserBalance {
    constructor(public id:number,
                public amount:number,
                public currency:string) {

    }

}