import {UserItem} from "./user";
/**
 * Created by duncmcm on 13/12/2016.
 */
export class UserTeam {

    public points:number;
    public user:UserItem;
    public midfielder:any;
    public forward:any;
    public defender:any;
    public goalkeeper:any;
    public flex:any;
    public entryStatus:string;
    public name:string;
    public players:Array<number>;

    public midfielderPoints:number;
    public forwardPoints:number;
    public defenderPoints:number;
    public goalkeeperPoints:number;
    public flexPoints:number;

    /**
     * Just used locally in the betting slip view
     */
    public contest:any;
    public entryFee:number;

    constructor() {

    }
}