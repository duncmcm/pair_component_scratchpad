var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 20/12/2016.
 */
//import * as _ from "lodash";
var core_1 = require("@angular/core");
var DataFilterPipe = (function () {
    function DataFilterPipe() {
    }
    DataFilterPipe.prototype.transform = function (array, query) {
        if (query) {
            return null; //_.filter(array, row=>row.name.indexOf(query) > -1);
        }
        return array;
    };
    DataFilterPipe = __decorate([
        core_1.Pipe({
            name: "dataFilter"
        })
    ], DataFilterPipe);
    return DataFilterPipe;
})();
exports.DataFilterPipe = DataFilterPipe;
//# sourceMappingURL=data.filter.pipe.js.map