var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
/**
 * Created by duncmcm on 19/12/2016.
 */
var core_1 = require("@angular/core");
var dataTableComponent = (function () {
    function dataTableComponent(http) {
        this.http = http;
        this.filterQuery = "";
        this.rowsOnPage = 10;
        this.sortBy = "email";
        this.sortOrder = "asc";
        this.sortByWordLength = function (a) {
            return a.city.length;
        };
    }
    dataTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get("./data/data.json")
            .subscribe(function (data) {
            setTimeout(function () {
                _this.data = data.json();
            }, 1000);
        });
    };
    dataTableComponent.prototype.toInt = function (num) {
        return +num;
    };
    dataTableComponent = __decorate([
        core_1.Component({
            selector: 'leaderboard',
            templateUrl: 'datatable.template.html'
        })
    ], dataTableComponent);
    return dataTableComponent;
})();
exports.dataTableComponent = dataTableComponent;
//# sourceMappingURL=datatable.component.js.map