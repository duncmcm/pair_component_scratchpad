/**
 * Created by duncmcm on 19/12/2016.
 */
import {Component, OnInit} from "@angular/core";
import {Http} from "@angular/http";

@Component({
    selector: 'leaderboard',
    templateUrl: 'datatable.template.html'
})
export class dataTableComponent implements OnInit {

    public data;
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "email";
    public sortOrder = "asc";

    constructor(private http: Http) {
    }

    ngOnInit(): void {
        this.http.get("./data/data.json")
            .subscribe((data)=> {
                setTimeout(()=> {
                    this.data = data.json();
                }, 1000);
            });
    }

    public toInt(num: string) {
        return +num;
    }

    public sortByWordLength = (a: any) => {
        return a.city.length;
    }

}