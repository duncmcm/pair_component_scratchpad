/**
 * Created by duncmcm on 19/12/2016.
 */
import { NgModule }      from '@angular/core';
import { CommonModule }      from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";
import { HttpModule } from "@angular/http";
import { DataFilterPipe } from './data.filter.pipe';

import { dataTableComponent }   from './datatable.component';

@NgModule({
    imports:      [
        CommonModule,
        DataTableModule,
        FormsModule,
        HttpModule
    ],
    declarations: [ dataTableComponent , DataFilterPipe],
    exports: [dataTableComponent]
})

export class DemoModule { }