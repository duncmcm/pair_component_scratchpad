var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var core_1 = require('@angular/core');
var userteam_1 = require('../models/userteam');
var Parse = require('parse');
var Subject_1 = require('rxjs/Subject');
var bettingslip_1 = require('../models/bettingslip');
var UserTeamService = (function () {
    function UserTeamService() {
        this.teams = [];
        this.teamSource = new Subject_1.Subject();
        this.teamAnnounce = this.teamSource.asObservable();
    }
    UserTeamService.prototype.ngOnInit = function () {
    };
    /**
     * getUserTeamEntriesInBettingSlip:
     * - ToDo: Returns all UserTeams in the Betting slip via the bettingSlip.entries
     */
    UserTeamService.prototype.getUserTeamEntriesInBettingSlip = function (slipId) {
        var that = this;
        var query = new Parse.Query('BettingSlip');
        query.equalTo('objectId', slipId);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (slip) {
                        console.log('Slip received with ID ' + slip.id + " entries " + slip.get('entries'));
                        var bs = new bettingslip_1.BettingSlip();
                        bs.entries = slip.get('entries');
                        bs.id = slip.id;
                        for (var i = 0; i < bs.entries.length; i++) {
                            that.getUserTeamsWithID(bs.entries[i]);
                        }
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Slip With ID ' + error);
            }
        });
    };
    UserTeamService.prototype.getUserTeamsWithID = function (id) {
        var that = this;
        var query = new Parse.Query('UserTeam');
        query.include('goalkeeper');
        query.include('defender');
        query.include('midfielder');
        query.include('forward');
        query.include('flex');
        query.include('contest');
        query.equalTo('objectId', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (userteam) {
                        console.log('UserTeam received with ID ' + userteam.id + " gk points " + userteam.get('goalkeeperPoints'));
                        var team = new userteam_1.UserTeam();
                        team.points = userteam.get('points');
                        team.midfielder = userteam.get('midfielder');
                        team.goalkeeper = userteam.get('goalkeeper');
                        team.defender = userteam.get('defender');
                        team.forward = userteam.get('forward');
                        team.flex = userteam.get('flex');
                        team.midfielderPoints = userteam.get('midfielderPoints');
                        team.goalkeeperPoints = userteam.get('goalkeeperPoints');
                        team.defenderPoints = userteam.get('defenderPoints');
                        team.forwardPoints = userteam.get('forwardPoints');
                        team.flexPoints = userteam.get('flexPoints');
                        team.user = userteam.get('user');
                        team.name = userteam.get('name');
                        team.players = userteam.get('players');
                        team.contest = userteam.get('contest');
                        that.teamSource.next(team);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting UserTeam With ID ' + error);
            }
        });
    };
    UserTeamService = __decorate([
        core_1.Injectable()
    ], UserTeamService);
    return UserTeamService;
})();
exports.UserTeamService = UserTeamService;
//# sourceMappingURL=UserTeamService.js.map