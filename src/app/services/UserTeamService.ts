/**
 * Created by duncmcm on 13/12/2016.
 */
import * as constants from '../constants/constants';
import {Injectable, OnInit, Inject} from '@angular/core';
import { UserTeam } from '../models/userteam';
import * as Parse from 'parse';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import { BettingSlip } from '../models/bettingslip';

@Injectable()
export class UserTeamService implements OnInit {

    public teams:UserTeam[] = [];
    public team:UserTeam;

    public teamSource = new Subject<UserTeam>();
    public teamAnnounce = this.teamSource.asObservable();

    ngOnInit() {

    }

    /**
     * getUserTeamEntriesInBettingSlip:
     * - ToDo: Returns all UserTeams in the Betting slip via the bettingSlip.entries
     */
    getUserTeamEntriesInBettingSlip(slipId:string) {
        var that = this;
        var query = new Parse.Query('BettingSlip');
        query.equalTo('objectId', slipId);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((slip) => {
                        console.log('Slip received with ID ' + slip.id + " entries " + slip.get('entries'));
                        let bs = new BettingSlip();
                        bs.entries = slip.get('entries');
                        bs.id = slip.id;

                        for (var i=0; i < bs.entries.length; i++) {
                            that.getUserTeamsWithID(bs.entries[i]);
                        }
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Slip With ID ' + error);
            }
        });
    }

    getUserTeamsWithID(id:string) {
        var that = this;
        var query = new Parse.Query('UserTeam');
        query.include('goalkeeper');
        query.include('defender');
        query.include('midfielder');
        query.include('forward');
        query.include('flex');
        query.include('contest');
        query.equalTo('objectId', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((userteam) => {  // Leave entries [] for now
                        console.log('UserTeam received with ID ' + userteam.id + " gk points " + userteam.get('goalkeeperPoints'));
                        let team = new UserTeam();
                        team.points = userteam.get('points');
                        team.midfielder = userteam.get('midfielder');
                        team.goalkeeper = userteam.get('goalkeeper');
                        team.defender = userteam.get('defender');
                        team.forward = userteam.get('forward');
                        team.flex = userteam.get('flex');
                        team.midfielderPoints = userteam.get('midfielderPoints');
                        team.goalkeeperPoints = userteam.get('goalkeeperPoints');
                        team.defenderPoints = userteam.get('defenderPoints');
                        team.forwardPoints = userteam.get('forwardPoints');
                        team.flexPoints = userteam.get('flexPoints');
                        team.user = userteam.get('user');
                        team.name = userteam.get('name');
                        team.players = userteam.get('players');
                        team.contest = userteam.get('contest');

                        that.teamSource.next(team);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting UserTeam With ID ' + error);
            }
        });
    }

}