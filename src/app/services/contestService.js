var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var core_1 = require('@angular/core');
var contest_1 = require('../models/contest');
var Parse = require('parse');
var moment = require('moment');
var userteam_1 = require("../models/userteam");
var Subject_1 = require('rxjs/Subject');
var ContestService = (function () {
    function ContestService() {
        this.contests = [];
        this.teams = [];
        this.contestSource = new Subject_1.Subject();
        this.contestAnnounce = this.contestSource.asObservable();
        this.contestUserTeamsSource = new Subject_1.Subject();
        this.contestUserTeamsAnnounce = this.contestUserTeamsSource.asObservable();
        this.selectedContestUserTeams = [];
    }
    ContestService.prototype.ngOnInit = function () {
    };
    ContestService.prototype.getContests = function () {
        var that = this;
        var query = new Parse.Query('Contest');
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (contest) {
                        console.log('Contest received in Service' + contest.id + " entryFee " + contest.get('entryFee'));
                        var ct = new contest_1.Contest();
                        ct.id = contest.id;
                        ct.prizes = contest.get('prizes');
                        ct.entries = contest.get('entries');
                        ct.fixtures = contest.get('fixtures');
                        ct.name = contest.get('name');
                        ct.entryFee = contest.get('entryFee');
                        ct.prizeType = contest.get('prizeType');
                        ct.status = contest.get('status');
                        ct.contestType = contest.get('contestType');
                        ct.startDate = that.formatTime(contest.get('startDate'));
                        ct.startDateLong = that.formatLongTime(contest.get('startDate'));
                        ct.startDateDayFormat = that.formatDayTime(contest.get('startDate'));
                        ct.startDateTime = that.formatDateTime(contest.get('startDate'));
                        that.contests.push(ct); // Provide option to be used locally
                        that.contestSource.next(ct);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Contests ' + error);
            }
        });
    };
    ContestService.prototype.getContestWithId = function (id) {
        var that = this;
        var query = new Parse.Query('Contest');
        query.equalTo('objectId', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (contest) {
                        //console.log('Contest received ' +contest.id +" name " +contest.get('entryFee'));
                        var ct = new contest_1.Contest();
                        ct.id = contest.id;
                        ct.prizes = contest.get('prizes');
                        ct.entries = contest.get('entries');
                        ct.fixtures = contest.get('fixtures');
                        ct.name = contest.get('name');
                        ct.entryFee = contest.get('entryFee');
                        ct.prizeType = contest.get('prizeType');
                        ct.contestType = contest.get('contestType');
                        ct.startDate = that.formatTime(contest.get('startDate'));
                        ct.startDateLong = that.formatLongTime(contest.get('startDate'));
                        ct.startDateDayFormat = that.formatDayTime(contest.get('startDate'));
                        ct.startDateTime = that.formatDateTime(contest.get('startDate'));
                        that.contestSource.next(ct);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Contests ' + error);
            }
        });
    };
    ContestService.prototype.getUserTeamsForContest = function (contestId) {
        var that = this;
        that.selectedContestUserTeams = [];
        var query = new Parse.Query('Contest');
        query.equalTo('objectId', contestId);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (contest) {
                        //console.log('Contest received ' +contest.id +" name " +contest.get('entryFee'));
                        var entries = [];
                        entries = contest.get('entries');
                        for (var i = 0; i < entries.length; i++) {
                            that.getUserTeamsWithID(entries[i]);
                        }
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Contests ' + error);
            }
        });
    };
    ContestService.prototype.getUserTeamsWithID = function (id) {
        var that = this;
        var query = new Parse.Query('UserTeam');
        query.equalTo('objectId', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (userteam) {
                        var team = new userteam_1.UserTeam();
                        team.points = userteam.get('points');
                        team.name = userteam.get('name');
                        that.selectedContestUserTeams.push(team);
                        that.contestUserTeamsSource.next(team);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting UserTeam With ID ' + error);
            }
        });
    };
    /**
     * formatTime -
     * @param time
     */
    ContestService.prototype.formatTime = function (time) {
        var date = moment(time).format('DD-MM-YY');
        return date.toString();
    };
    /**
     * formatDateTime -
     * @param time
     */
    ContestService.prototype.formatDateTime = function (time) {
        var date = moment(time).format('DD-MM-YY h:mm a');
        return date.toString();
    };
    /**
     * formatTimeLong -
     * @param time
     * @returns {string}
     */
    ContestService.prototype.formatLongTime = function (time) {
        var date = moment(time).format('MMMM Do YYYY, h:mm a');
        return date.toString();
    };
    /**
     * formatLongTime
     * @param time
     * @returns {string}
     */
    ContestService.prototype.formatDayTime = function (time) {
        var date = moment(time).format('dddd h:mm a');
        return date.toString();
    };
    /**
     *
     * @param item - Contest
     * Contest {"prizes":[10],"name":"Test Contest"}
     */
    ContestService.prototype.addContest = function () {
    };
    /**
     * Use for when parse is down or during development
     */
    ContestService.prototype.getDummyData = function () {
        var dummyContests = [];
        var now = moment().format('DD-MM-YY');
        var ct = new contest_1.Contest();
        ct.prizes = [10];
        ct.fixtures = [855312];
        ct.name = 'Leon Loves';
        ct.entryFee = 5;
        ct.prizeType = 'Winner Takes All';
        ct.contestType = 'Head to Head';
        ct.startDate = now;
        dummyContests.push(ct);
        this.contests = dummyContests;
    };
    ContestService = __decorate([
        core_1.Injectable()
    ], ContestService);
    return ContestService;
})();
exports.ContestService = ContestService;
//# sourceMappingURL=contestService.js.map