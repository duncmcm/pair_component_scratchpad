/**
 * Created by duncmcm on 07/12/2016.
 */
import * as constants from '../constants/constants';
import {Injectable, OnInit, Inject} from '@angular/core';
import { Contest } from '../models/contest';
import * as Parse from 'parse';
import * as moment from 'moment';
import {UserTeam} from "../models/userteam";
import {Fixture} from "../models/fixture";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ContestService implements OnInit {

    public contests:Contest[] = [];
    public teams:UserTeam[] = [];

    public contestSource = new Subject<Contest>();
    public contestAnnounce = this.contestSource.asObservable();

    public contestUserTeamsSource = new Subject<UserTeam>();
    public contestUserTeamsAnnounce = this.contestUserTeamsSource.asObservable();

    public selectedContest:Contest;
    public selectedContestUserTeams:Array<UserTeam> = [];

    ngOnInit() {

    }

    getContests() {
        var that = this;
        let query = new Parse.Query('Contest');
        query.find({
            success: function(results) {
                if (results) {
                    results.forEach((contest) => {
                        console.log('Contest received in Service' +contest.id +" entryFee " +contest.get('entryFee'));
                        let ct = new Contest();
                        ct.id = contest.id;
                        ct.prizes = contest.get('prizes');
                        ct.entries = contest.get('entries');
                        ct.fixtures = contest.get('fixtures');
                        ct.name = contest.get('name');
                        ct.entryFee = contest.get('entryFee');
                        ct.prizeType = contest.get('prizeType');
                        ct.status = contest.get('status');
                        ct.contestType = contest.get('contestType');
                        ct.startDate = that.formatTime(contest.get('startDate'));
                        ct.startDateLong = that.formatLongTime(contest.get('startDate'));
                        ct.startDateDayFormat = that.formatDayTime(contest.get('startDate'));
                        ct.startDateTime = that.formatDateTime(contest.get('startDate'));

                        that.contests.push(ct); // Provide option to be used locally
                        that.contestSource.next(ct);

                    });
                }
            },
            error: function(error) {
                console.log('Error Getting Contests ' +error);
            }
        });
    }

    getContestWithId(id:string) {
        var that = this;
        let query = new Parse.Query('Contest');
        query.equalTo('objectId', id);
        query.find({
            success: function(results) {
                if (results) {
                    results.forEach((contest) => {
                        //console.log('Contest received ' +contest.id +" name " +contest.get('entryFee'));
                        let ct = new Contest();
                        ct.id = contest.id;
                        ct.prizes = contest.get('prizes');
                        ct.entries = contest.get('entries');
                        ct.fixtures = contest.get('fixtures');
                        ct.name = contest.get('name');
                        ct.entryFee = contest.get('entryFee');
                        ct.prizeType = contest.get('prizeType');
                        ct.contestType = contest.get('contestType');
                        ct.startDate = that.formatTime(contest.get('startDate'));
                        ct.startDateLong = that.formatLongTime(contest.get('startDate'));
                        ct.startDateDayFormat = that.formatDayTime(contest.get('startDate'));
                        ct.startDateTime = that.formatDateTime(contest.get('startDate'));

                        that.contestSource.next(ct);

                    });
                }
            },
            error: function(error) {
                console.log('Error Getting Contests ' +error);
            }
        });
    }

    getUserTeamsForContest(contestId:string) {
        var that = this;
        that.selectedContestUserTeams = [];
        let query = new Parse.Query('Contest');
        query.equalTo('objectId', contestId);
        query.find({
            success: function(results) {
                if (results) {
                    results.forEach((contest) => {
                        //console.log('Contest received ' +contest.id +" name " +contest.get('entryFee'));
                        let entries = [];
                        entries = contest.get('entries');

                        for (var i=0; i < entries.length; i++) {
                            that.getUserTeamsWithID(entries[i]);
                        }
                    });
                }
            },
            error: function(error) {
                console.log('Error Getting Contests ' +error);
            }
        });

    }

    getUserTeamsWithID(id:string) {
        var that = this;
        var query = new Parse.Query('UserTeam');
        query.equalTo('objectId', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((userteam) => {
                        let team = new UserTeam();
                        team.points = userteam.get('points');
                        team.name = userteam.get('name');

                        that.selectedContestUserTeams.push(team);
                        that.contestUserTeamsSource.next(team);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting UserTeam With ID ' + error);
            }
        });
    }


    /**
     * formatTime -
     * @param time
     */
    formatTime(time:string) {
        let date = moment(time).format('DD-MM-YY');
        return date.toString();
    }

    /**
     * formatDateTime -
     * @param time
     */
    formatDateTime(time:string) {
        let date = moment(time).format('DD-MM-YY h:mm a');
        return date.toString();
    }

    /**
     * formatTimeLong -
     * @param time
     * @returns {string}
     */
    formatLongTime(time:string) {
        let date = moment(time).format('MMMM Do YYYY, h:mm a');
        return date.toString();
    }

    /**
     * formatLongTime
     * @param time
     * @returns {string}
     */
    formatDayTime(time:string) {
        let date = moment(time).format('dddd h:mm a');
        return date.toString();
    }

    /**
     *
     * @param item - Contest
     * Contest {"prizes":[10],"name":"Test Contest"}
     */
    addContest() {

    }

    /**
     * Use for when parse is down or during development
     */
    getDummyData() {
        let dummyContests = [];
        let now = moment().format('DD-MM-YY');
        let ct = new Contest();
        ct.prizes = [10];
        ct.fixtures = [855312];
        ct.name = 'Leon Loves';
        ct.entryFee = 5;
        ct.prizeType = 'Winner Takes All';
        ct.contestType = 'Head to Head';
        ct.startDate = now;
        dummyContests.push(ct);
        this.contests = dummyContests;
    }
}
