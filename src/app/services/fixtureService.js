var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var core_1 = require('@angular/core');
var fixture_1 = require('../models/fixture');
var Parse = require('parse');
var moment = require('moment');
var Subject_1 = require('rxjs/Subject');
var FixtureService = (function () {
    function FixtureService() {
        this.fixtures = [];
        this.fixtureSource = new Subject_1.Subject();
        this.fixtureAnnounce = this.fixtureSource.asObservable();
    }
    FixtureService.prototype.ngOnInit = function () {
    };
    FixtureService.prototype.getFixtures = function () {
        var that = this;
        var query = new Parse.Query('Fixture');
        query.find({
            success: function (results) {
                var result = [];
                if (results) {
                    results.forEach(function (fixture) {
                        console.log('Fixture received ' + fixture.id + " name " + fixture.get('teamNameHome'));
                        var fix = new fixture_1.Fixture();
                        fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
                        fix.fixtureID = fixture.get('fixtureID');
                        fix.gameweek = fixture.get('gameweek');
                        fix.teamIDAway = fixture.get('teamIDAway');
                        fix.teamNameHome = fixture.get('teamNameHome');
                        fix.status = fixture.get('status');
                        fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
                        fix.teamNameAway = fixture.get('teamNameAway');
                        fix.teamIDHome = fixture.get('teamIDHome');
                        fix.competitionID = fixture.get('competitionID');
                        fix.seasonID = fixture.get('seasonID');
                        result.push(fix);
                    });
                }
                that.fixtures = result;
            },
            error: function (error) {
                console.log('Error Getting Fixtures ' + error);
            }
        });
    };
    FixtureService.prototype.getFixtureWithID = function (id) {
        var that = this;
        var query = new Parse.Query('Fixture');
        query.equalTo('fixtureID', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (fixture) {
                        console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHome'));
                        var fix = new fixture_1.Fixture();
                        fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
                        fix.fixtureID = fixture.get('fixtureID');
                        fix.gameweek = fixture.get('gameweek');
                        fix.teamIDAway = fixture.get('teamIDAway');
                        fix.teamNameHome = fixture.get('teamNameHome');
                        fix.status = fixture.get('status');
                        fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
                        fix.teamNameAway = fixture.get('teamNameAway');
                        fix.teamIDHome = fixture.get('teamIDHome');
                        fix.competitionID = fixture.get('competitionID');
                        fix.seasonID = fixture.get('seasonID');
                        that.fixtureSource.next(fix);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Fixtures With ID ' + error);
            }
        });
    };
    FixtureService.prototype.getFixtureWithStatus = function (status) {
        var that = this;
        var query = new Parse.Query('Fixture');
        query.equalTo('status', status);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (fixture) {
                        console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHomeAbbr'));
                        var fix = new fixture_1.Fixture();
                        fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
                        fix.fixtureID = fixture.get('fixtureID');
                        fix.gameweek = fixture.get('gameweek');
                        fix.teamIDAway = fixture.get('teamIDAway');
                        fix.teamNameHome = fixture.get('teamNameHome');
                        fix.status = fixture.get('status');
                        fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
                        fix.teamNameAway = fixture.get('teamNameAway');
                        fix.teamIDHome = fixture.get('teamIDHome');
                        fix.competitionID = fixture.get('competitionID');
                        fix.seasonID = fixture.get('seasonID');
                        fix.teamScoreAway = fixture.get('teamScoreAway');
                        fix.teamScoreHome = fixture.get('teamScoreHome');
                        that.fixtureSource.next(fix);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Fixtures With Status ' + error);
            }
        });
    };
    //loadFixture(fixture:Fixture) {
    //    console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHome'));
    //    let fix = new Fixture();
    //    fix.teamNameAwayAbbr = fixture.get('teamNameAwayAbbr');
    //    fix.fixtureID = fixture.get('fixtureID');
    //    fix.gameweek = fixture.get('gameweek');
    //    fix.teamIDAway = fixture.get('teamIDAway');
    //    fix.teamNameHome = fixture.get('teamNameHome');
    //    fix.status = fixture.get('status');
    //    fix.teamNameHomwAbbr = fixture.get('teamNameHomwAbbr');
    //    fix.teamNameAway = fixture.get('teamNameAway');
    //    fix.teamIDHome = fixture.get('teamIDHome');
    //    fix.competitionID = fixture.get('competitionID');
    //    fix.seasonID = fixture.get('seasonID');
    //    return fix;
    //}
    /**
     * formatTime -
     * @param time
     */
    FixtureService.prototype.formatTime = function (time) {
        var date = moment(time).format('DD-MM-YY');
        //date.format('DD-MM-YY');
        return date.toString();
    };
    /**
     *
     * @param item - Contest
     * Contest {"prizes":[10],"name":"Test Contest"}
     */
    FixtureService.prototype.addFixture = function () {
    };
    FixtureService = __decorate([
        core_1.Injectable()
    ], FixtureService);
    return FixtureService;
})();
exports.FixtureService = FixtureService;
//# sourceMappingURL=fixtureService.js.map