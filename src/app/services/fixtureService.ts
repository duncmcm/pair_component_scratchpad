/**
 * Created by duncmcm on 12/12/2016.
 */
import * as constants from '../constants/constants';
import {Injectable, OnInit, Inject} from '@angular/core';
import { Fixture } from '../models/fixture';
import * as Parse from 'parse';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class FixtureService implements OnInit {

    public fixtures:Fixture[] = [];
    public fixture:Fixture;

    public fixtureSource = new Subject<Fixture>();
    public fixtureAnnounce = this.fixtureSource.asObservable();

    ngOnInit() {

    }

    getFixtures() {
        var that = this;
        var query = new Parse.Query('Fixture');
        query.find({
            success: function (results) {
                let result = [];
                if (results) {
                    results.forEach((fixture) => {  // Leave entries [] for now
                        console.log('Fixture received ' + fixture.id + " name " + fixture.get('teamNameHome'));
                        let fix = new Fixture();
                        fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
                        fix.fixtureID = fixture.get('fixtureID');
                        fix.gameweek = fixture.get('gameweek');
                        fix.teamIDAway = fixture.get('teamIDAway');
                        fix.teamNameHome = fixture.get('teamNameHome');
                        fix.status = fixture.get('status');
                        fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
                        fix.teamNameAway = fixture.get('teamNameAway');
                        fix.teamIDHome = fixture.get('teamIDHome');
                        fix.competitionID = fixture.get('competitionID');
                        fix.seasonID = fixture.get('seasonID');

                        result.push(fix);
                    });
                }
                that.fixtures = result;
            },
            error: function (error) {
                console.log('Error Getting Fixtures ' + error);
            }
        });
    }

    getFixtureWithID(id:number) {
        var that = this;
        var query = new Parse.Query('Fixture');
        query.equalTo('fixtureID', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((fixture) => {
                        console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHome'));
                        let fix = new Fixture();
                        fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
                        fix.fixtureID = fixture.get('fixtureID');
                        fix.gameweek = fixture.get('gameweek');
                        fix.teamIDAway = fixture.get('teamIDAway');
                        fix.teamNameHome = fixture.get('teamNameHome');
                        fix.status = fixture.get('status');
                        fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
                        fix.teamNameAway = fixture.get('teamNameAway');
                        fix.teamIDHome = fixture.get('teamIDHome');
                        fix.competitionID = fixture.get('competitionID');
                        fix.seasonID = fixture.get('seasonID');

                        that.fixtureSource.next(fix);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Fixtures With ID ' + error);
            }
        });
    }

    getFixtureWithStatus(status:string) {
        var that = this;
        var query = new Parse.Query('Fixture');
        query.equalTo('status', status);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((fixture) => {
                        console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHomeAbbr'));
                        let fix = new Fixture();
                        fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
                        fix.fixtureID = fixture.get('fixtureID');
                        fix.gameweek = fixture.get('gameweek');
                        fix.teamIDAway = fixture.get('teamIDAway');
                        fix.teamNameHome = fixture.get('teamNameHome');
                        fix.status = fixture.get('status');
                        fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
                        fix.teamNameAway = fixture.get('teamNameAway');
                        fix.teamIDHome = fixture.get('teamIDHome');
                        fix.competitionID = fixture.get('competitionID');
                        fix.seasonID = fixture.get('seasonID');
                        fix.teamScoreAway = fixture.get('teamScoreAway');
                        fix.teamScoreHome = fixture.get('teamScoreHome');

                        that.fixtureSource.next(fix);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Fixtures With Status ' + error);
            }
        });
    }

    //loadFixture(fixture:Fixture) {
    //    console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHome'));
    //    let fix = new Fixture();
    //    fix.teamNameAwayAbbr = fixture.get('teamNameAwayAbbr');
    //    fix.fixtureID = fixture.get('fixtureID');
    //    fix.gameweek = fixture.get('gameweek');
    //    fix.teamIDAway = fixture.get('teamIDAway');
    //    fix.teamNameHome = fixture.get('teamNameHome');
    //    fix.status = fixture.get('status');
    //    fix.teamNameHomwAbbr = fixture.get('teamNameHomwAbbr');
    //    fix.teamNameAway = fixture.get('teamNameAway');
    //    fix.teamIDHome = fixture.get('teamIDHome');
    //    fix.competitionID = fixture.get('competitionID');
    //    fix.seasonID = fixture.get('seasonID');
    //    return fix;
    //}

    /**
     * formatTime -
     * @param time
     */
    formatTime(time:string) {
        let date = moment(time).format('DD-MM-YY');
        //date.format('DD-MM-YY');
        return date.toString();
    }

    /**
     *
     * @param item - Contest
     * Contest {"prizes":[10],"name":"Test Contest"}
     */
    addFixture() {

    }

}