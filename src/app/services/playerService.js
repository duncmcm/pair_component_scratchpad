var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var core_1 = require('@angular/core');
var player_1 = require('../models/player');
var Parse = require('parse');
var Subject_1 = require('rxjs/Subject');
var PlayerService = (function () {
    function PlayerService() {
        this.playerSource = new Subject_1.Subject();
        this.playerAnnounce = this.playerSource.asObservable();
        this.allPlayerSource = new Subject_1.Subject();
        this.allPlayerAnnounce = this.allPlayerSource.asObservable();
    }
    PlayerService.prototype.ngOnInit = function () {
    };
    PlayerService.prototype.getPlayers = function () {
        var that = this;
        var query = new Parse.Query('Player');
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (player) {
                        //console.log('Player received ' + player.id + " name " + player.get('lastName'));
                        var p = new player_1.Player();
                        p.playerID = player.get('playerID');
                        p.lastName = player.get('lastName');
                        p.position = player.get('position');
                        p.teamID = player.get('teamID');
                        that.playerSource.next(p);
                    });
                }
                that.allPlayerSource.next(true);
            },
            error: function (error) {
                console.log('Error Getting Players ' + error);
            }
        });
    };
    PlayerService.prototype.getPlayersForUserTeam = function (team) {
        var that = this;
        var query = new Parse.Query('Player');
        query.containedIn('playerID', team.players);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (player) {
                        var p = new player_1.Player();
                        p.playerID = player.get('playerID');
                        p.lastName = player.get('lastName');
                        p.position = player.get('position');
                        p.teamID = player.get('teamID');
                        that.playerSource.next(p);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Players With ID ' + error);
            }
        });
    };
    PlayerService.prototype.getPlayerWithID = function (id) {
        var that = this;
        var query = new Parse.Query('Player');
        query.equalTo('playerID', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (player) {
                        console.log('Player received with ID ' + player.id + " name " + player.get('teamNameHome'));
                        var p = new player_1.Player();
                        p.playerID = player.get('playerID');
                        p.lastName = player.get('lastName');
                        p.position = player.get('position');
                        that.playerSource.next(p);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Players With ID ' + error);
            }
        });
    };
    PlayerService = __decorate([
        core_1.Injectable()
    ], PlayerService);
    return PlayerService;
})();
exports.PlayerService = PlayerService;
//# sourceMappingURL=playerService.js.map