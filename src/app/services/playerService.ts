/**
 * Created by duncmcm on 15/12/2016.
 */
import * as constants from '../constants/constants';
import {Injectable, OnInit, Inject} from '@angular/core';
import { Player } from '../models/player';
import * as Parse from 'parse';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import {UserTeam} from "../models/userteam";
import {PlayerEvent} from "../models/playerevent";

@Injectable()
export class PlayerService implements OnInit {

    public playerSource = new Subject<Player>();
    public playerAnnounce = this.playerSource.asObservable();

    public allPlayerSource = new Subject<boolean>();
    public allPlayerAnnounce = this.allPlayerSource.asObservable();

    ngOnInit() {

    }

    getPlayers() {
        var that = this;
        var query = new Parse.Query('Player');
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((player) => {  // Leave entries [] for now
                        //console.log('Player received ' + player.id + " name " + player.get('lastName'));
                        let p = new Player();
                        p.playerID = player.get('playerID');
                        p.lastName = player.get('lastName');
                        p.position = player.get('position');
                        p.teamID = player.get('teamID');

                        that.playerSource.next(p);
                    });
                }
                that.allPlayerSource.next(true);
            },
            error: function (error) {
                console.log('Error Getting Players ' + error);
            }
        });
    }

    getPlayersForUserTeam(team:UserTeam) {
        var that = this;
        var query = new Parse.Query('Player');
        query.containedIn('playerID', team.players);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((player) => {
                        let p = new Player();
                        p.playerID = player.get('playerID');
                        p.lastName = player.get('lastName');
                        p.position = player.get('position');
                        p.teamID   = player.get('teamID');

                        that.playerSource.next(p);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Players With ID ' + error);
            }
        });
    }

    getPlayerWithID(id:number) {
        var that = this;
        var query = new Parse.Query('Player');
        query.equalTo('playerID', id);
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach((player) => {  // Leave entries [] for now
                        console.log('Player received with ID ' + player.id + " name " + player.get('teamNameHome'));
                        let p = new Player();
                        p.playerID = player.get('playerID');
                        p.lastName = player.get('lastName');
                        p.position = player.get('position');

                        that.playerSource.next(p);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Players With ID ' + error);
            }
        });
    }

}