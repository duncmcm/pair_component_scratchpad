var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var user_1 = require('../models/user');
var core_1 = require('@angular/core');
var Parse = require('parse');
var Subject_1 = require('rxjs/Subject');
/**
 * @ngdoc service
 * @name UserService
 * @description
 *  Provides a CRUD service for User classes stored in the parse server
 */
var UserService = (function () {
    function UserService() {
        this.userLoginConfirmedSource = new Subject_1.Subject();
        this.userLoginConfirmed = this.userLoginConfirmedSource.asObservable();
        this.userSource = new Subject_1.Subject();
        this.userConfirmed = this.userSource.asObservable();
    }
    Object.defineProperty(UserService.prototype, "loggedInUser", {
        get: function () {
            return this._loggedInUser;
        },
        set: function (value) {
            if (value) {
                this._loggedInUser = value;
                this.userLoginConfirmedSource.next(value);
            }
        },
        enumerable: true,
        configurable: true
    });
    UserService.prototype.ngOnInit = function () {
    };
    UserService.prototype.getUsers = function () {
        var that = this;
        var query = new Parse.Query("User");
        query.include('bettingSlip');
        query.find({
            success: function (results) {
                if (results) {
                    results.forEach(function (user) {
                        //console.log('User received in UserService' + user.id + " username " + user.get('username'));
                        var u = new user_1.UserItem();
                        u.id = user.get('id');
                        u.username = user.get('username');
                        u.email = user.get('email');
                        u.balance = user.get('balance');
                        u.password = user.get('password');
                        u.bettingSlip = user.get('bettingSlip');
                        that.userSource.next(u);
                    });
                }
            },
            error: function (error) {
                console.log('Error Getting Users');
            }
        });
    };
    UserService = __decorate([
        core_1.Injectable()
    ], UserService);
    return UserService;
})();
exports.UserService = UserService;
//# sourceMappingURL=userService.js.map