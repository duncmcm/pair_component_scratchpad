import {UserItem} from '../models/user';
import {UserBalance} from '../models/user_balance';
import {Injectable, OnInit} from '@angular/core';
import { Inject } from '@angular/core'
import * as constants from '../constants/constants';
import * as Parse from 'parse';
import { Subject } from 'rxjs/Subject';

/**
 * @ngdoc service
 * @name UserService
 * @description
 *  Provides a CRUD service for User classes stored in the parse server
 */
@Injectable()
export class UserService implements OnInit {

    private userLoginConfirmedSource = new Subject<UserItem>();
    userLoginConfirmed = this.userLoginConfirmedSource.asObservable();

    private userSource = new Subject<UserItem>();
    userConfirmed = this.userSource.asObservable();

    _loggedInUser:UserItem;
    set loggedInUser(value) {
        if (value) {
            this._loggedInUser = value;
            this.userLoginConfirmedSource.next(value);
        }
    }

    get loggedInUser() {
        return this._loggedInUser;
    }

    constructor() {

    }

    ngOnInit() {

    }

    getUsers() {
        var that = this;
        var query = new Parse.Query("User");
        query.include('bettingSlip');
        query.find({
            success: function(results) {
                if (results) {
                    results.forEach((user)=> {
                        //console.log('User received in UserService' + user.id + " username " + user.get('username'));
                        let u = new UserItem();
                        u.id = user.get('id');
                        u.username = user.get('username');
                        u.email = user.get('email');
                        u.balance = user.get('balance');
                        u.password = user.get('password');
                        u.bettingSlip = user.get('bettingSlip');

                        that.userSource.next(u);
                    });
                }
            },
            error: function(error) {
                console.log('Error Getting Users');
            }
        });

    }
}
